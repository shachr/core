/**
 * Created by shachar on 5/2/15.
 */
module.exports = function( conf ){

    this.send = function( options, callback ){

        var sendgrid = require('sendgrid')(conf.auth.user, conf.auth.pass);
        var email = new sendgrid.Email();
        options.to.forEach( function( e ){
            email.addTo( e );
        });

        email.setFrom(options.from);
        email.setSubject(options.subject);

        email.setHtml(options.html);

        return sendgrid.send(email, callback);
    };

};