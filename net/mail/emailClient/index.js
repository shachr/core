/**
 * Created by shachar on 5/2/15.
 */


/**
 * Email client service
 * Usage:
 * var emailClient = new EmailClient( EmailClient.providers.sendGrid );
 *
 * @param provider - provider to use
 * @param conf - configuration of the mail server
 */
var EmailClient = module.exports = function( provider, conf ){
    this.provider = {}
    require("./impl/"+provider).call(this.provider, conf);
    this.conf = conf;
};

EmailClient.prototype = {
    /**
     * Utility function to send emails
     * @param options - nodemailer options for the sendMail function
     * @param callback optional
     */
    send: function( options, callback ){
        options.to = options.to.split(/,|;/);
        return this.provider.send(options, callback);
    }
};

EmailClient.providers = {
    sendGrid: "sendGridImpl"
};


