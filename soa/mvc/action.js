/**
 * Created by shachar on 6/5/15.
 */
var ActionExecuter              = require("./action-executer");

var Action = module.exports = function( controller,handler ){
    this._handler = handler;
    this._controller = controller;
    this._app = controller._app;
    this._filters = [];
};

Action.prototype = {
    invoke: function( model, callback){
        return new ActionExecuter( this ).execute(model, callback);
    },

    name: function(value){
        if(arguments.length){
            this._name=value;
            this._controller._aliases[name] = this;
            return this;
        }
        return this._name;
    },

    filters: function( value ){
        if(arguments.length == 1) {
            this._filters = value;
            return this;
        }
        return this._filters;
    },

    model: function( value ){
        if(arguments.length == 1)
            this._model = value;
        return this;
    }
};
