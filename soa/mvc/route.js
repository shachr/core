/**
 * Created by shachar on 6/5/15.
 */
var utils                   = require("./utils");
var RouteParams             = require("./route-params");
var xregex                  = require("xregexp").XRegExp;

var Route = module.exports = function Route( area, path ){
    this._options = { path: path, constraints: [], area: area };
    this._area = area;
    this._options.placeHolders = utils.placeHolderPatternToRegex(area._options.path + path, area._app.getRoutePatterns().toMap(), { allOptional: true });
};

Route.prototype = {
    options: function(){
        return this._options;
    },
    getArea: function(){
      return this._area;
    },
    defaults: function(value){
      this._options.defaults = value;
      return this;
  },
  constraints: function(value){
      this._options.constraints.push(value);
      return this;
  },
  match: function( path ){
      return xregex.test(path, xregex(this._options.placeHolders.xpattern));
  },
  getRouteParams: function( path ) {
      return new RouteParams( this, path );
  }

};
