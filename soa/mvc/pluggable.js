/**
 * Created by shachar on 6/8/15.
 */
var util                = require("util");
var async               = require("async");
var events              = require("events");
var extendify           = require("extendify");
var extend              = extendify({ inPlace:false });

var Pluggable = module.exports = function( ){ this._plugins = [];  };
var Plugin = Pluggable.Plugin = function(){
    events.EventEmitter.call(this)
};
util.inherits(Plugin,events.EventEmitter);
Plugin.prototype.app = function(value){
    if(arguments.length == 1)
        this._app = value;
    return this._app;
};

Pluggable.prototype = {
    app: function(value){
        if(arguments.length == 1)
            this._app = value;
        return this._app;
    },

    insert: function(inx, pInstance, arg ){

        if( !(pInstance instanceof Plugin) ) {
            //throw new TypeError("not instanceof Pluggable.Plugin");
            var instance = arg ? new Plugin(arg) : new Plugin();
            pInstance.call(instance,arg);
            pInstance = instance;
        }
        pInstance.app(this._app);
        this._plugins.splice(inx,0,pInstance);
        return pInstance;
    },

    use: function( pInstance, arg ){
        return this.insert(0,pInstance,arg);
    },

    unuse: function( pluginInstance ){
        var inx = this._plugins.indexOf( pluginInstance );
        if( inx > -1 )
            this._plugins.splice(inx,1);
        return inx > -1;
    },

    clear: function(){
        this._plugins = [];
    },


    getPluggable: function(){
        return {
            insert: this.insert.bind(this),
            use: this.use.bind(this),
            unuse: this.unuse.bind(this),
            clear: this.clear.bind(this)
        }
    }

};