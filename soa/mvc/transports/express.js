/**
 * Created by shachar on 6/5/15.
 */
var extendify               = require("extendify");
var extend                  = extendify({ inPlace: false });
var util                    = require("util");
var Plugin                  = require("../pluggable").Plugin;

var ExpressTransport = module.exports = function ExpressTransport( express ){
    //Plugin.call(this);
    this.on("begin-request", adapter.bind(this));
    express.use(middleware.bind(this))
};
//util.inherits(ExpressTransport,Plugin);


function adapter ( event, envalope ){
    var req = envalope.request;
    event.result({
        request: {
            path: req.path,
            headers: req.headers,
            //cookies: req.cookies,
            params: extend(req.query, req.body)
        }
    });
}

function middleware ( req, res, next ){
    this.app().execute(req, function (err, result) {
        if(err){
            console.error(err);
            next(err);
        }
        else {
            res.status(200).send(result);
        }
    });
}