/**
 * Created by shachar on 6/5/15.
 */
var util                = require("util");
var SchemaPlugin        = require("./plugins/schema-plugin");
var EmittablePlugin     = require("../emittable-plugin");

var ModelBinder = module.exports = function( app ){
    this.app( app );
    EmittablePlugin.call(this);
    this.use(new SchemaPlugin()); // default
};
util.inherits(ModelBinder,EmittablePlugin);

ModelBinder.prototype.bind = function( modelConfig, params, callback ){

    this.emit("model-binding", { modelConfig:modelConfig, params: params}, function(err){
        callback(err, params);
    }.bind(this));
};