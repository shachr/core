/**
 * Created by shachar on 6/8/15.
 */
var validator           = require('is-my-json-valid');
var JaySchema           = require("jayschema");
var Ajv                 = require("ajv");
var JSONPath            = require('jsonpath');
var util                = require("util");

var Plugin              = require("../../pluggable").Plugin;
var ValidationError     = require("../../errors/validation-error");


var SchemaPlugin = module.exports = function(){
    Plugin.call(this);
    this.on("model-binding", jaySchema);
};

util.inherits(SchemaPlugin, Plugin);

function ajvSchema(event, envalope){
    var ajv = Ajv({allErrors:true, beautify:false, _debug:false }); // options can be passed

    var schema = envalope.modelConfig._schema;
    var params = envalope.params;

    envalope.modelConfig._compliedSchema = envalope.modelConfig._compliedSchema || ajv.compile(schema);
    var validate = envalope.modelConfig._compliedSchema;

    var valid = validate(params);
    if (!valid){
        var errs = validate.errors;
        var filteredOut = [];
        var validationErrors = [];
        if(errs && errs.length) {
            errs.forEach(function (err) {
                err.field = err.errorPath;
                console.log(err);
                if (err.keyword != 'additionalProperties')
                    validationErrors.push(err);
                else
                    filteredOut.push(err);
            });
        }

        if(validationErrors.length)
            event.error( new ValidationError(validationErrors) );
        else {
            //filter(params);
            event.result(params);
        }
    };
}
function jaySchema(event, envalope){
    var js = new JaySchema();

    var schema = envalope.modelConfig._schema;
    var params = envalope.params;

    js.validate(params, schema, function(errs) {

        var validationErrors = [];
        if(errs && errs.length) {
            var filteredOut = [];
            errs.forEach(function (err) {
                err.field = err.field.replace(/^data\./,""); //remove 'data.' prefix
                if (err.message != 'has additional properties')
                    validationErrors.push(err);
                else
                    filteredOut.push(err);
            });
        }

        if(validationErrors.length)
            event.error( new ValidationError(validationErrors) );
        else {
            js.filter(params);
            event.result(params);
        }
    });

}
function modelBindingWithFilter( event, envalope ){
    var modelConfig = envalope.modelConfig;
    var params = envalope.params;

    var filter = validator.filter(modelConfig._schema, { greedy: true, verbose: true });//, filter:true
    var validate = validator(modelConfig._schema, { greedy: true, verbose: true });//, filter:true
    validate(params);

    var validationErrors = [];
    if(validate.errors && validate.errors.length) {
        var filteredOut = [];
        validate.errors.forEach(function (err) {
            err.field = err.field.replace(/^data\./,""); //remove 'data.' prefix
            if (err.message != 'has additional properties')
                validationErrors.push(err);
            else
                filteredOut.push(err);
        });
    }

    if(validationErrors.length)
        event.error( new ValidationError(validationErrors) );
    else {
        filter(params);
        event.result(params);
    }
}
