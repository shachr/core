/**
 * Created by shachar on 6/3/15.
 *
 */

/*
 var model = require("mvc").model;
 module.exports = model.create(function(){

     this.firstname = this.prop().schema({ "type": "string", "format": "phone-us" });
     this.lastname = this.prop().schema({ "type": "string", "format": "phone-us" });
     this.birthdate = this.prop();

     this.on("validate", function(){

     });
 });
 */

var util                    = require("util");
var events                  = require("events");
var Prop                    = require("./prop");
var extendify               = require("extendify");
var extend                  = extendify({inPlace: false, arrays: 'merge' });


module.exports = Model;


function Model( app ){
    this._app = app;
}

//util.inherits(Model, events.EventEmitter);

Model.prototype.schema = function( value ){
    this._schema = value;
};


Model.prototype.prop = function(){
    return new Prop();
};

