/**
 * Created by shachar on 6/9/15.
 */

var async           = require("async");
var utils           = require("./utils");

var defaultCallbackTimeout = 3000;
var ActionExecuter = module.exports = function ActionExecuter( action ){
    this._action            = action;
    this._app               = action._app;
    this._filters           = this._action.filters();
    this._callbackTimeout   = this._app.get("callback-timeout") || defaultCallbackTimeout;
}

ActionExecuter.prototype = {
    execute: function( model, callback ){

        this.filterContext = {
            model: model,
            error: function( value ){
                if(arguments.length == 1) {
                    callback(value);
                    this._closed = true;
                }
                else
                    return this._error;
            },
            result: function( value ){
                if(arguments.length == 1) {
                    callback(null, value);
                    this._closed = true;
                }
                else
                    return this._result;
            }
        };

        // execute 'authorizing' filters


        this.invokeAuthorize( this.filterContext, function(){
            if(this.filterContext._closed)return;

            // execute 'executing' filters
            this.invokeExecuting( this.filterContext, function(){
                if(this.filterContext._closed)return;

                // execute action
                this._action._handler.call(this._controller, this.filterContext.model, utils.timedoutCallback(function( err ,res ){
                    this.filterContext._error = err;
                    this.filterContext._result = res;

                    var invokeCompleted = function (){
                        if(this.filterContext._closed)return;

                        // execute 'completed' filters
                        this.invokeCompleted( this.filterContext, function(){
                            if(this.filterContext._closed)return;
                            callback( this.filterContext.error(), this.filterContext.result() );
                        }.bind(this) );

                    }.bind(this);

                    if(!err)
                    // execute 'executed' filters
                        this.invokeExecuted( this.filterContext, invokeCompleted );


                    else
                    // execute 'error' filters
                        this.invokeError( this.filterContext, invokeCompleted );



                }.bind(this), this._callbackTimeout));

            }.bind(this));

        }.bind(this))
    },

    executeFilters: function(filterType, filterContext, next ){
        var filters = this._filters.filter( function( filter ){ return filter.listeners( filterType ).length > 0; });
        if(filters.length == 0)
            next();
        else {
            var inx=0;
            async.doWhilst(
                function( proceed ){
                    var filter = filters[inx++];
                    filter.emit(filterType, filterContext, utils.timedoutCallback( proceed, this._callbackTimeout ));
                }.bind(this),

                function(){
                    return (inx<filters.length && !filterContext._closed);
                },

                function(){
                    if( !filterContext._closed)next();
                }
            );
        }
    },

    invokeAuthorize: function( filterContext, next ){
        this.executeFilters("authorizing",filterContext, next);
    },

    invokeExecuting: function( filterContext, next ){
        this.executeFilters("executing",filterContext, next);
    },

    invokeExecuted: function( filterContext, next ){
        this.executeFilters("executed",filterContext, next);
    },

    invokeError: function( filterContext, next ){
        this.executeFilters("error",filterContext, next);
    },

    invokeCompleted: function( filterContext, next ){
        this.executeFilters("completed",filterContext, next);
    }
}
