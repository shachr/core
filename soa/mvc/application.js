/**
 * Created by shachar on 6/4/15.
 */
var util                    = require("util");
var extendify               = require("extendify");
var Area                    = require('./area');
var ModelBinder             = require("./model-binder/index");
var CallExecuter            = require("./call-executer");
var RoutePatterns           = require("./route-patterns");
var EmittablePlugin         = require("./emittable-plugin");
var extend                  = extendify({inPlace: false});

var Application = module.exports = function Application( ){
    EmittablePlugin.call(this);
    this._areas = [];
    this._options = [];
    this._modelBinder = new ModelBinder( this );
    this._callExecuter = new CallExecuter( this );
    this._routePatterns = new RoutePatterns( this );
};



Application.prototype.getRoutePatterns= function(){
    return this._routePatterns;
};

Application.prototype.getModelBinderFactory= function(){
    return this._modelBinder.getPluggable();
};

Application.prototype.use= function( name, value ){
    if( typeof name == 'function'){
        this._callExecuter.use(name, value);
    }
    else
        this._options[name] = value;
};

Application.prototype.get= function( name ){
    return this._options[name];
};

Application.prototype.area= function( path ){
    var area = new Area(this,path);
    this._areas.push( area );
    return area;
};

Application.prototype.execute= function( request, callback ){
    var envalope = { request: request }; //todo: hang on the domain!
    this._callExecuter.execute( envalope, callback );
};


