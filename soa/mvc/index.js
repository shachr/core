/**
 * Created by shachar on 6/4/15.
 */

/*

 var app = require("mvc")();

 app.schema.addFormat("phone-us", function(value) {
     var PHONE_US_REGEXP = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
     if (PHONE_US_REGEXP.test(value)) { return null; }
     return 'must be a US phone number';
 });

 var router = app.router("/")
     .controller( "./controllers/{name}-controller" )
     .model( "./models/{name}-request" );

router.
    .route("{controller}/{action}")
    .defaults({ controller: "home", action: "index" })
    .constraints(fn);

 app.router("/admin")
    .controller( "./admin/controllers/{name}-controller" )
    .model( "./admin/models/{name}-request" );


 app.router("/console")
    .controller( "./console/controllers/{name}-controller" )
    .model( "./console/models/{name}-request" );


 */

var Application     = require('./application');

module.exports = function(){
    return new Application();
};
