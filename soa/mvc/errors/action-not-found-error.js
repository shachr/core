/**
 * Created by shachar on 6/4/15.
 */
var util            = require("util");
var MvcError        = require("./mvc-error");

var ActionNotFoundError = module.exports = function ActionNotFoundError( actionName ){
    MvcError.call(this, "action not found: " + actionName);
};

util.inherits(ActionNotFoundError, MvcError);