/**
 * Created by shachar on 6/4/15.
 */
var util            = require("util");
var MvcError        = require("./mvc-error");

var RouteNotFound = module.exports = function RouteNotFound(routeName){
    MvcError.call(this, "route not found: " + routeName);
};

util.inherits(RouteNotFound, MvcError);