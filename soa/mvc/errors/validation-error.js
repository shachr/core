/**
 * Created by shachar on 6/4/15.
 */
var util            = require("util");
var MvcError        = require("./mvc-error");

var ValidationError = module.exports = function ValidationError( errors ){
    MvcError.call(this, "validation errors");
    this.validationErrors( errors );
};

util.inherits(ValidationError, MvcError);

ValidationError.prototype.validationErrors = function(errors){
    if(arguments.length == 1)
        this._validationErrors = errors;
    return this._validationErrors;
};