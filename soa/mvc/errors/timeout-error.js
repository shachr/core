/**
 * Created by shachar on 6/4/15.
 */
var util            = require("util");
var MvcError        = require("./mvc-error");

var TimeoutError = module.exports = function TimeoutError(){
    MvcError.call(this, "asynchronous operation timeout");
};

util.inherits(TimeoutError, MvcError);