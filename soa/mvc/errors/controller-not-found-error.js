/**
 * Created by shachar on 6/4/15.
 */
var util            = require("util");
var MvcError        = require("./mvc-error");

var ControllerNotFoundError = module.exports = function ControllerNotFoundError( controllerName ){
    MvcError.call(this, "controller not found: " + controllerName);
};

util.inherits(ControllerNotFoundError, MvcError);