/**
 * Created by shachar on 6/4/15.
 */
var util            = require("util");

var MvcError = module.exports = function MvcError( errMsg ){
    Error.call(this);
    Error.captureStackTrace(this, arguments.callee); // capture stack trace
    this.message = errMsg;
};

util.inherits(MvcError, Error);