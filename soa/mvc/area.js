/**
 * Created by shachar on 6/3/15.
 */
var util                    = require("util");
var events                  = require("events");
var Controller              = require("./controller");
var Model                   = require("./model");
var Route                   = require("./route");
var utils                   = require("./utils");
var MvcError                = require("./errors/mvc-error");
var path                    = require("path");
var xregex                  = require("xregexp").XRegExp;

var Area = module.exports = function Area( app, options ){
    this._app = app;
    if(typeof options == 'string')
        options = { path: options };

    // ensure area ends with "/"
    if(options.path[options.path.length-1] != "/")
        options.path +="/";

    this._options = options;
    this._controllers = {};
    this._models = {};
    this._routes = [];
    events.EventEmitter.call(this);
    return this;
}
util.inherits(Area, events.EventEmitter);

function functionName(fun) {
    var ret = fun.toString();
    ret = ret.substr('function '.length);
    ret = ret.substr(0, ret.indexOf('('));
    return ret;
}

Area.prototype.findController = function(controller){
    return this.controllers()[controller];
};

Area.prototype.findRoute = function( path ){

    if(path.indexOf(this._options.path) == 0){
        for(var inx=0; inx < this._routes.length; inx++){
            var route = this._routes[inx];
            if( route.match(path) ) {
                return route.getRouteParams( path );
            }
        }
    }
    return false;
};

Area.prototype.options = function(){
    return this._options;
};

Area.prototype.controllers = function(){
    return this._controllers;
};

Area.prototype.routes = function(){
    return this._routes;
};

Area.prototype.route = function( path ){
    var route = new Route(this, path);
    this._routes.push(route);
    return route;
};

Area.prototype.controller = function( name, controller ){
    var placeHolders =  utils.placeHolderPatternToRegex(controller+".js",{ _default: "[\\w\-\.]+" });

    name = utils.resolveRelativePath(name,1);
    var files = require("../../io/directory").getFilesSync( name, placeHolders.pattern );
    files.forEach( function( file ){

        var match = xregex.exec(file, xregex(placeHolders.xpattern));

        var controllerConfigurator = require( path.join(name,file) );
        var controller = new Controller(match.name, this);
        controllerConfigurator.call( controller );
        var controllerName = controller.name();
        this._controllers[ controllerName ] = controller;

    }.bind(this));

    return this;
};
//
//Area.prototype.model = function( name, model ){
//
//    var placeHolders =  utils.placeHolderPatternToRegex(model+".js",{ _default: "[\\w\-\.]w+" });
//    name = utils.resolveRelativePath(name,1);
//
//    var files = require("../../io/directory").getFilesSync( name, placeHolders.pattern );
//    files.forEach( function( file ){
//
//        var match = xregex.exec(file, xregex(placeHolders.xpattern));
//
//        var modelConfigurator = require( path.join(name,file) );
//        var model = new Model(this._app, match.name);
//        modelConfigurator.call( model );
//        this._models[ match.name ] = model;
//    }.bind(this));
//
//    return this;
//};
