/**
 * Created by shachar on 6/5/15.
 */
var xregex                  = require("xregexp").XRegExp;
var extendify               = require("extendify");
var extend                  = extendify({inPlace: false});

var RouteParams = module.exports = function RouteParams ( route, path ){
    this._options = { route: route,  path: path, area: route.getArea() };
    this._route = route;
    this._area = route.getArea();
}

RouteParams.prototype = {
    options: function(){
      return this._options;
    },
    getRoute: function(){
        return this._route;
    },
    getArea: function(){
      return this._area;
    },
    getParams: function( ){
        var path = this._options.path;
        var placeHolders = this._options.route._options.placeHolders;

        var params = {};

        var match = xregex.exec(path, xregex(placeHolders.xpattern));
        for(var key in match)
            if (match.hasOwnProperty(key) && !/\d+|index|input/.test(key))
                if (match[key] != undefined)
                    params[key] = match[key];

        return extend(this._options.route._options.defaults || {},{ area: this.getArea().options().path }, params);
    }
}