/**
 * Created by shachar on 6/5/15.
 */
var stackTrace          = require("stack-trace");
var path                = require("path");
var TimeoutError        = require("./errors/timeout-error");
var self = this;

function escapeForRegex(value){

    if(value)
        return value.replace(".","\\.")
            .replace("$","\\$")
            .replace("^","\\^")
            .replace("-","\\-")
            .replace("=","\\=")
            .replace("[","\\[").replace("]","\\]")
            .replace("{","\\{").replace("}","\\}")
            .replace("(","\\(").replace(")","\\)");
    return value;
}

module.exports = {
    placeHolderPatternToRegex: function ( str, predefinedPatterns, options ){

        options = options || {};

        var placeHolders = [];
        var nonPhPattern = /(\})([^\{]+)/g;
        var phPattern = /\{([^\}]+)\}?/g;

        str = str.replace(nonPhPattern, function(m,g,g2){
            return "}("+escapeForRegex(g2) + (options.allOptional ? "?" : "" ) +")"
        });

        var pattern = str.replace(phPattern, function(match, grp1){

            if(predefinedPatterns && grp1.indexOf(":") > -1){
                var parts = grp1.split(":");
                grp1 = parts[0];

                var predefinePatternName = parts[1];
                var predefinePattern = predefinedPatterns[predefinePatternName];
                if(!predefinePattern)
                    throw Error("predefined pattern not found: "+ predefinePatternName);
                return "("+predefinePattern+")?";
            }else {
                return "(" + ( predefinedPatterns && predefinedPatterns._default ? predefinedPatterns._default : ".+" ) + ")?";
            }
        });

        var xpattern = str.replace(phPattern, function(match, grp1){

            if(predefinedPatterns && grp1.indexOf(":") > -1){

                var parts = grp1.split(":");
                grp1 = parts[0];
                placeHolders.push(grp1);

                var predefinePatternName = parts[1];
                var predefinePattern = predefinedPatterns[predefinePatternName];
                if(!predefinePattern)
                    throw Error("predefined pattern not found: "+ predefinePatternName);
                return "(?<" + grp1 + ">" + predefinePattern + ")?"
            }

            placeHolders.push(grp1);
            return "(?<" + grp1 + ">"+( predefinedPatterns && predefinedPatterns._default ? predefinedPatterns._default : ".+" ) +")?";
        });

        placeHolders.pattern = new RegExp( "^"+ pattern + "$" );
        placeHolders.xpattern = "^"+xpattern+"$";
        return placeHolders;
    },

    resolveRelativePath: function( relativePath ){
        var trace = stackTrace.get(arguments.callee.caller);
        var caller = trace[0];

        if(relativePath.indexOf(".") == 0)
            relativePath = path.resolve( path.dirname( caller.getFileName()), relativePath );

        return relativePath;
    },

    timedoutCallback: function(callback, timeout){
        var run, timer;
        if(!timeout)throw new Error("required argument: timeout");

        run = function ( err, res ) {
            clearTimeout(timer);
            timer = null;
            if (err instanceof TimeoutError) {
                res.call(this, err);
            }
            else{
                callback.call(this, err, res);
            }
        };

        timer = setTimeout(run, timeout, new TimeoutError(), callback);
        return run;
    }
};

