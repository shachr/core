/**
 * Created by shachar on 6/9/15.
 */
var RoutePatterns = module.exports = function(){
    this._patterns = {};
};

RoutePatterns.prototype = {
    use: function( name, pattern ){
        this._patterns[name] = pattern;
    },
    get: function(name){
        return this._patterns[name];
    },
    toMap: function(){
        return this._patterns;
    }
};
