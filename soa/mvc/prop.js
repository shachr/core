/**
 * Created by shachar on 6/5/15.
 */

var Prop = module.exports = function Prop(){

}

Prop.prototype = {
    schema: function( schema ){
        this._schema = schema;
        return this;
    },
    name: function(name){
        this._name = name;
        return this;
    },
    value: function( value ){
        if(arguments.length == 1)
            this._value = value;
        return this._value;
    }
};