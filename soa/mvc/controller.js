/**
 * Created by shachar on 6/3/15.
 */
/*
 router.controller( "home", function(){

     this.index = this.action(function( commentRequest  ){

     })
        .on("authorizing", function(){

        })
        .on("executing", function(){

        })
        .on("executed", function(){

        })
        .on("error", function(){

        })
        .on("completed", function(){

        });


      this.index = this.action(function( commentRequest  ){

      })
        .filters([
            CacheFilter({ key :'commentRequest.id'}),
            new CacheFilter({ key :'commentRequest.id'}),
            CacheFilter.create({ key :'commentRequest.id'}),
            cacheBuilder().key('commentRequest.id').build()
        ]);


     this.on("action-not-found", function( context ){

     })

 });
 */

var util            = require("util");
var events          = require("events");
var Action          = require("./action");

var Controller = module.exports = function Controller( name, area ){
    events.EventEmitter.call(this);
    this._name      = name;
    this._area      = area;
    this._app       = area._app;
    this._aliases   = {};
};

util.inherits(Controller, events.EventEmitter);
Controller.prototype.action= function(fn){
    return new Action(this,fn);
};

Controller.prototype.name= function(value){
    if(arguments.length ==1 )
        this._name = value;
    return this._name;
};