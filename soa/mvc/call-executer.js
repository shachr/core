/**
 * Created by shachar on 6/8/15.
 */
var Model                       = require("./model");
var RouteNotFoundError          = require("./errors/route-not-found-error");
var ControllerNotFoundError     = require("./errors/controller-not-found-error");
var ActionNotFoundError         = require("./errors/action-not-found-error");
var ValidationError             = require("./errors/validation-error");
var TimeoutError                = require("./errors/timeout-error");
var EmittablePlugin             = require("./emittable-plugin");
var util                        = require("util");
var Action                      = require("./action");

var CallExecuter = module.exports = function CallExecuter( app ){
    this.app( app );
    EmittablePlugin.call(this);
};
util.inherits(CallExecuter,EmittablePlugin);

CallExecuter.prototype.findRoute = function( path ){
    var app = this._app;
    var applicableroutes = [];
    for(var inx=0;inx<app._areas.length;inx++){
        var area = app._areas[inx];
        var route = area.findRoute( path );
        if(route)
            applicableroutes.push( route );
    }

    if(applicableroutes.length == 0)
        return false;
    if(applicableroutes.length == 1)
        return applicableroutes[0];
    else
    {
        applicableroutes.sort( function(a,b){
            return (a.getArea().options().path > b.getArea().options().path)? -1 : 1;
        });
        return applicableroutes[0];
    }
};

CallExecuter.prototype.execute = function( envalope, done ){

    var endRequest = function endRequest(err, res){
        if(err){
            if(err instanceof ValidationError) {
                this.emit("validation-errors", err, function (innerErr, innerRes) {
                    if (innerRes)
                        done(null,innerRes);
                    else
                        done(innerErr || err);
                });

                return;
            }
            if(err instanceof TimeoutError){
                this.emit("request-timeout", err, function(innerErr, innerRes){
                    if (innerRes)
                        done(null,innerRes);
                    else
                        done(innerErr || err);
                });
                return;
            }

            return done(err);
        }

        this.emit("end-request", envalope, function(innerErr, innerRes){
            if(innerErr)
                done(innerErr);
            else
                done(null, innerRes || res);
        });
    }.bind(this);

    this.emit("begin-request", envalope, function(err,evtRes){

        if(err)
            return endRequest( err );

        if(evtRes)
            envalope = evtRes;

        //todo: return proper errors (create base error and relevant inherited errors!!)
        var route = this.findRoute( envalope.request.path );
        if(!route)
            return endRequest(new RouteNotFoundError(envalope.request.path));


        var area = route.getArea();
        var routeParams = route.getParams();
        var controllerName = routeParams.controller;
        var controller = area.findController( controllerName );
        if(!controller)
            return endRequest(new ControllerNotFoundError( controllerName ));

        var actionName = route.getParams().action;
        var action = controller[ actionName ] || controller._aliases[ actionName ];

        if(!action || !(action instanceof Action) ) {
            var hasSubscribers = controller.emit("action-not-found", envalope, endRequest);
            if(!hasSubscribers)
                return endRequest(new ActionNotFoundError(actionName));
            return;
        }

        var model = new Model(this);
        action._model.call( model );
        this._app._modelBinder.bind( model, envalope.request.params, function(err, requestModel){
            if(err)
                return endRequest(err);
            return action.invoke(requestModel, endRequest);
        } );
    }.bind(this));
};