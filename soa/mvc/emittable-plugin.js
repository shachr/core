/**
 * Created by shachar on 6/8/15.
 */
var util        = require("util");
var async       = require("async");
var Pluggable   = require("./pluggable");

var EmittablePlugin = module.exports = function EmittablePlugin(){
    Pluggable.call(this);
};
util.inherits(EmittablePlugin,Pluggable);


EmittablePlugin.prototype.emit = function(evt, envalope, done){
    var inx = 0;
    var handled = false;

    if(this._plugins.length == 0)
        return done(null);

    var eventArgs = {
        name: evt,
        plugin: undefined,
        next: undefined,
        error: function( value ){
            if(arguments.length == 1) {
                this._error = value;
                handled = true;
                this.next(value);
            }
            return this._error;
        },
        result: function(value){
            if(arguments.length == 1) {
                this._result = value;
                handled = true;
                this.next(null,value);
            }
            return this._result;
        }
    };

    async.doWhilst(
        function(next){
            eventArgs.next = next;
            var plugin = this._plugins[inx++];
            eventArgs.plugin = plugin;

            if(!plugin.emit(evt, eventArgs, envalope))
                next();

        }.bind(this),

        function(){
            return !handled && inx<this._plugins.length;
        }.bind(this),

        function(){
            done(eventArgs._error,eventArgs._result);
        }
    )
};