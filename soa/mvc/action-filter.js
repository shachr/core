/**
 * Created by shachar on 6/9/15.
 */

var util            = require("util");
var events          = require("events");

var Filter = module.exports = function Filter(){
    events.EventEmitter.call(this);
};

util.inherits(Filter, events.EventEmitter);