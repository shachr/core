/**
 * Created by shachar on 5/24/15.
 */

var os                      = require('os');
var cluster                 = require('cluster');

    //if the worker dies, restart it.
    //cluster.on('exit', function(worker){
    //    console.log('Worker ' + worker.id + ' died..');
    //    cluster.fork(); //restart
    //});

module.exports = new Cluster();


function Cluster(){}

Cluster.prototype.setup = function( config ){
    var numCPUs         = config.workers || os.cpus().length;

    console.log("setting up the cluster");
    cluster.setupMaster({
        exec: 'worker.js',
        args: ['--use', 'https'],
        silent: true
    });


    console.log("starting the cluster");
    startWorkers( numCPUs );
};

function startWorkers( numOfWorkers ){
    for (var i = 0; i < numOfWorkers; i++) {
        console.log("starting worker #" + i);
        cluster.fork();
    }
}