/**
 * Created by shachar on 5/16/15.
 */
var _                   = require("lodash");
var cluster             = require('cluster');
var http                = require('http');
var app                 = require("./app");
var MvcApplication      = require("../app");


var WebServer = module.exports = function( config ){
    this._config = config;
    this._app = new MvcApplication(app());
};

WebServer.prototype = {

    app: function(){
        return this._app;
    },

    start: function( ){
        if(this._isRunning ) throw new Error("web server is already running!");
        this._isRunning = true;
        this._server = createWorker( this._app, this._config );
        return this;
    },

    stop: function( cb ){
        if(this._isStopping ) throw new Error("web server is already stopping!");
        this._isStopping = true;

        return this;
    },

    restart: function(){
        this.stop( function(){
            this.start(this._app);
        }.bind(this));
        return this;
    }
};



function createWorker( app, config ){

    var port            = normalizePort(config.port || process.env.PORT || '3000');

    app.set('port', port);

    /**
     * Create HTTP server.
     */
    var server = http.createServer(app);

    /**
     * Listen on provided port, on all network interfaces.
     */

    server.listen(port);
    server.on('error', onError);
    server.on('listening', onListening);

    /**
     * Event listener for HTTP server "error" event.
     */

    function onError(error) {
        if (error.syscall !== 'listen') {
            throw error;
        }

        var bind = typeof port === 'string'
            ? 'Pipe ' + port
            : 'Port ' + port;

        // handle specific listen errors with friendly messages
        switch (error.code) {
            case 'EACCES':
                console.error(bind + ' requires elevated privileges');
                process.exit(1);
                break;
            case 'EADDRINUSE':
                console.error(bind + ' is already in use');
                process.exit(1);
                break;
            default:
                throw error;
        }
    }

    /**
     * Event listener for HTTP server "listening" event.
     */

    function onListening() {
        var addr = server.address();
        var bind = typeof addr === 'string'
            ? 'pipe ' + addr
            : 'port ' + addr.port;

        console.log('listening to ' + bind);
    }

    process.on('SIGTERM', function () {
        server.close(function () {
            console.warn("\n-----------------------\nGracefully shutting down from SIGINT (Ctrl-C)\n-----------------------\n");
            process.exit(0);
        });
    });

    process.on('uncaughtException', function (err) {
        console.error(err);
    });

}


/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}