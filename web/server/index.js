/**
 * Created by shachar on 5/16/15.
 */
/*
 this actualy accept a web-server config name and start it up
 Usage:

 */

// force default env to development
process.env.NODE_ENV        = process.env.NODE_ENV || "development";

var _                       = require("lodash");
var Cluster                 = require("./cluster");
var WebServer               = require("./web-server");
var extendify               = require("extendify");
extend = extendify({
 inPlace: false
});

var defaultConfig = {
  port: 3000,
  cluster: {
    enabled: false,
    workers: "auto"
  }
};

module.exports = function( config ) {
 config = extend( config || {}, defaultConfig);
 return config.cluster.enabled ? new Cluster(config) : new WebServer(config);
};

module.exports.defaultConfig = defaultConfig;