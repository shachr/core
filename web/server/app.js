var compression                     = require('compression');
var express                         = require('express');
var path                            = require('path');
//var favicon                         = require('serve-favicon');
var morgan                          = require('morgan');
var cookieParser                    = require('cookie-parser');
var bodyParser                      = require('body-parser');
var useragent                       = require('express-useragent');


function createApplication( config ) {
    var app = express();

    // view engine setup
    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'hjs');

    // use
    app.use(require('express-domain-middleware'));
    app.use(compression()); // compress all requests
    app.use(useragent.express());
    app.use(morgan('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({extended: false}));
    app.use(cookieParser());
    app.use(expressMVC());

    app.use(function(req,res,next){
        res.status(200).send({ ok: 1 });
    });
    return app;
}


module.exports = createApplication;
