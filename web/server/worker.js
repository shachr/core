/**
 * Created by shachar on 5/24/15.
 */
var WebServer                   = require("./web-server");
var webConfig                   = require("core/configuration/configurationManager").getSection("web") || {};

var config = webConfig.webServer;
if(!config) throw new Error("web server configuration not found: " + name);

var webServer = module.exports = new WebServer(config);
webServer.start();


