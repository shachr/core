/**
 * Created by shachar on 5/9/15.
 */
var app         = null;
var initialized = false;
var webConfig   = configurationManager.getSection("web");
/*
"web":{

    "routeTable": {
        "enabled": true,
            "routes": [
            { "add": { "name": "default api", "url": "*", "module": "core/web/express-routing", "verbs": ["*"] } }
        ]
    }
}
 */

function initialize() {
    if(initialized)return;
    initialized = true;

    if (webConfig.routeTable && webConfig.routeTable.enabled) {
        if (webConfig.routeTable.routes && webConfig.routeTable.routes.length) {

            app = require("./app")();
            var server = require("./server")(app, {});
            webConfig.routeTable.routes.forEach(function (item) {
                // todo: merge items and add/remove/clear
                //if(item.add){
                //    var mdl = item.add.module;
                //
                //}
            });
        }
    }
}

module.exports = {
  getApplication: function(){
      initialize();
      return app;
  }
};

/*

 */