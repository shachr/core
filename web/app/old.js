/*
 Application folder structure:
 -----------------------------------------------
 .
 ├── app.js
 ├── bin
 │   └── www
 ├── package.json
 ├── public
 │   ├── images
 │   ├── javascripts
 │   └── stylesheets
 │       └── style.css
 ├── routes
 │   ├── account
 │       └── index.js
 └── views
 │   ├── account
 │       └── index.hjs
 */

var express         = require("express");
var session         = require('express-session');
var di              = require("di");
var acl             = require("../../security/acl");
var passport        = require('passport');
var LocalStrategy   = require('passport-local').Strategy;
var util            = require("util");

//https://github.com/tdegrunt/jsonschema
var Validator       = require('jsonschema').Validator;

var primitiveJsonSchemaTypes = ["array", "boolean","integer","number","null","object","string"];

var $injector = function(fn){
    return fn();
};

/**
 * @typedef AuthenticationOptions
 * @type {Object}
 * @property {boolean} enabled - indicates whether authentication feature is enabled.
 * @property {authenticate} authenticate - function that handles authentication
 * @property {getUserById} getUserById - indicates whether authentication feature is enabled.
 * @property {setUser} setUser - indicates whether authentication feature is enabled.
 *
 *
 * This function is handeling the authentication of a user
 * @function AuthenticationOptions~authenticate
 * @param {string} username
 * @param {string} password
 *
 *
 * This function is displayed as part of the Requester class.
 * @function AuthenticationOptions~getUserById
 * @param {string} username
 *
 *
 * This function is handeling the update of a user settings
 * @function AuthenticationOptions~setUser
 * @param {User} user
 *
 *
 * @typedef User
 * @type {Object}
 * @property {string} username - the user's name
 * @property {string} passwordHash - the hash value of the password
 */

var User = function(){};
User.prototype = {
    username: null,
    passwordHash: null
};

var AuthenticationOptions =  {
    enabled: false,
    authenticate: null,
    getUserById: null,
    setUser: null
};


var $authentication = {};

var validator = new Validator();

var apiEngine = (function( ) {

    return {
        injector: function( val ){
            $injector = val;
        },

        /**
         * Set or get the authentication options.
         * @function authenticationOptions
         * @param {AuthenticationOptions} options - the authentication options
         * @returns {AuthenticationOptions} authentication options
         */
        authenticationOptions: function( options ){
            if(!!options)
                $authenticationOptions = options;
            return $authentication;
        },

        express: function (app, filenameOrConfig) {

            var apiEngineConfig = {};
            if (arguments.length == 1) {
                apiEngineConfig = (configurationManager.getSection("web") || {}).routeTable;
            }
            else {
                switch (typeof filenameOrConfig) {

                    case 'string':
                        var readFileOptions = {};
                        apiEngineConfig = fs.readFileSync(filenameOrConfig, readFileOptions);
                        break;
                    case 'object':
                        apiEngineConfig = filenameOrConfig;
                        break;
                    default:
                        throw new Error("invalid parameter: filenameOrConfig");
                        break;
                }
            }

            configureSchema(app, apiEngineConfig.schema );
            configureRoutes(app, apiEngineConfig );
            if (apiEngineConfig.authentication && apiEngineConfig.authentication.enabled)
                configurePassport(app, apiEngineConfig.authentication);

        }
    };


    function configureSchema( app, config ){
        for(var key in config) {
            var container = config[ key ];
            container.forEach( function( schema ){
                validator.addSchema(schema, config.id);
            });
        }
    }

    function configurePassport( app, config ) {
        app.use(passport.initialize());

        passport.use( new LocalStrategy( $authentication.authenticate ) );
        passport.serializeUser( $authentication.saveUser );
        passport.deserializeUser( $authentication.findUserById );

        config.session = config.session || {};
        config.session.settings = config.session.settings || {};

        if (app.get('env') === 'production')
            config.session.settings.cookie.secure = true;

        app.use( session( config.session.settings ) );
        app.use( passport.session() );
    }

    function configureRoutes(app, config) {

        var routes = config.routes;

        // iterate all routes
        for (var routeName in routes) {
            var route = routes[ routeName ];

            // create different router per namespace
            var router = express.Router();

            // iterate all methods
            for (var methodName in route.methods) {
                var method = route.methods[ methodName ];

                // handling module is found by convention.
                var filename = methodName.replace(/[A-Z][^A-Z]+/, function( m ){
                    return "-" + m.toLowerCase() + "-route";
                });
                var filepath = [ process.cwd() + "/routes/", routeName, "/", filename ].join('');

                // route is dot delimited
                var route = ['/', methodName].join('');

                // create endpoint per verb decleration
                var handler = $injector( require(filepath) );
                var verb = method.verb || ["get", "post"];
                verb.forEach(function (v) {
                    if (!router[v]) throw new Error("verb is not supported: " + v);
                    router[v]( // verb
                        route, //reviews/postReview
                        middleware.bind // handler
                        (
                            {
                                app: app,
                                config: method,
                                handler: handler
                            }
                        )
                    );
                });

            }

            // register router.
            app.use("/" + routeName, router);
        }

    };

    function validation(schema, model) {
        // validate
        return validator.validate(model, schema).errors;
    }

    function authorize(user, path, schema, model, callback) {
        var roles = user.roles;


        // ensure sufficient permissions
        if (!acl.isAllowed(roles, path, ['x'])) {
            var ret = { error: { header: {errorCode: 403001, errorMessage: "insufficient permissions", resourceId: path } } };
            if (callback)
                callback(ret);
            return ret;
        }

        var removedProperties = [];
        for (var key in model) {
            if (callback && key == "format") continue; //todo: modify the code to accept depth !!!
            var fullpath = path + "/" + key;
            var prop = model[ key ];

            var schemaProperty = schema.properties[key];
            if(schemaProperty && !~primitiveJsonSchemaTypes.indexOf( schemaProperty.type ))
                schemaProperty = validator.schemas[ schemaProperty.type ];


            // remove the property from the model
            if (!schemaProperty) {
                delete model[ key ];
                removedProperties.push(fullpath);
            }
            else if (schemaProperty.type == 'object') {
                schemaProperty.id = schemaProperty.id || path + "/" + key;
                var ret = authorize(user, fullpath, schemaProperty, prop);
                if (callback && ret.error) {
                    callback(ret.error);
                    return;
                }

                if (ret.removedProperties && ret.removedProperties.length)
                    removedProperties.push.apply(removedProperties, ret.removedProperties);

                continue;
            }
            else if (!acl.isAllowed(roles, fullpath, ['x'])) {
                var ret = { error: { header: {errorCode: 403001, errorMessage: "insufficient permissions", resourceId: fullpath } } };
                if (callback)
                    callback(ret);
                return ret;
            }
        }

        if (callback)
            callback(null, { removedProperties: removedProperties });
        return { removedProperties: removedProperties };
    }

    function middleware(req, res, next) {
        try {
            var config = this.config;
            var handler = this.handler;

            // get the model
            var model = req.method == "GET" ? req.query : req.body;

            // schema
            var schemaKey = config.schema;

            // authentication
            // TBD: get user roles from cache
            var user = { roles: ['member'] };

            var callback = function (err, data) {

                // security error
                if (!!err) {
                    res.send(err);
                    return;
                }

                // support format
                var format = req.query.format || req.body.format;

                // validation of parameters
                var errors = validation(schema, model);
                if (errors && errors.length) {
                    var dispErrors = [];
                    errors.forEach(function (item) {
                        dispErrors.push(item.message);
                    });
                    res.send({ header: { errorCode: 402001, errorMessage: "Validation Error", validationErrors: dispErrors } });
                    return;
                }

                // invoke
                handler(
                    model,
                    function (err, result) {

                        if (!!err)
                            result = err;
                        else {
                            result.model = result.model || {}
                            result.model.header = result.model.header || {};
                            result.model.header.errorCode = 0;
                            result.model.header.ignoredProperties = (data || {}).removedProperties;
                        }

                        // todo: handle exceptions
                        // return response
                        switch (result.format || format) {
                            case "html":
                                res.render(result.view || schema.id.substring(1), result.model);
                                break;
                            default:
                                res.send(200, result.model);
                                break
                        }

                    }
                );
            };

            // find method schema
            var schema = validator.schemas[ schemaKey ];
            if (!schema) {
                var ret = { error: { header: {errorCode: 404001, errorMessage: "method not found" } } };
                if (callback)
                    callback(ret);
                return ret;
            }

            // authorization
            authorize(user, schemaKey, schema, model, callback);
        }catch(e){
            next(e);
        }
    }




})();
module.exports = apiEngine;