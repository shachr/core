/**
 * Created by shachar on 5/21/15.
 */
module.exports = {

    pre: function( fn ){
        return {
            type: "pre",
            handler: fn
        }
    },

    post: function( fn ){
        return {
            type: "post",
            handler: fn
        }
    },

    error: function( fn ){
        return {
            type: "error",
            handler: fn
        }
    }
}