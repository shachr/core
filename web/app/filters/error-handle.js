/**
 * Created by shachar on 5/21/15.
 */
var filterFactory      =   require("../filterFactory");
module.exports = filterFactory.error(function( filterContext, value, next ){

    if(typeof value == 'string')
        filterContext.routeContext.action = value;

    next();
});