/**
 * Created by shachar on 5/21/15.
 */
var filterFactory      =   require("../filterFactory");
module.exports = filterFactory.pre(function( filterContext, value, next ){

    var httpContext = filterContext.httpContext;

    if(typeof value == 'string'){
        if( value == httpContext.req.method )
            filterContext.skipAction();

    } else if (value instanceof Array){
        if( value.indexOf( httpContext.req.method ) == -1 )
            filterContext.skipAction();

    }
    else{
        filterContext.skipAction();

    }
});