/**
 * Created by shachar on 5/16/15.
 */
/*

Usage:
expressRouting.express( app );


 */
var MvcApplication          = require("./application");

//var plugins = [];
module.exports = {
    express: function( app, container ){
        //init( app );
        var mvcApp = new MvcApplication(app, container);
        return mvcApp;
    }
};
