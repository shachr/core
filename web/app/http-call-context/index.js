/**
 * Created by shachar on 5/16/15.
 */
module.exports = {
    middleware: function( req, res, next ){
        if(process.domain)
            process.domain.httpContext = {
                req: req,
                res: res,
                user: req.user
            };
        next()
    },

    current: function(){
        return process.domain.httpContext;
    }
};