/**
 * Created by shachar on 5/20/15.
 */
var Controller              = require("./controller");
var util                    = require("util");
var _                       = require("lodash");
var async                   = require("async");
var Weaver                  = require("./weaver");
var Container               = require("../../applicationContext/serviceLocator/container");
var HttpContext             = require("./http-call-context");


var MvcApplication = module.exports = function( app, container ){
    this.app = app;
    this.filters = [];
    this.filterNames = [];
    this.controllers = {};
    this.container = container || new Container();
    //
    //this.filter( "verb", require("./filters/verb") );
    //this.filter( "errorHandle", require("./filters/error-handle") );
    //this.filter( "actionName", require("./filters/action-name") );

    this.app.use ( middleware.bind(this) );
};

function middleware( req, res, done ){
    var httpContext = HttpContext.current();
    var parts = req._parsedUrl.pathname.split("/");
    var controllerName = parts[1];
    var actionName = parts[2] || "index";

    var actionInvokerBinded = actionInvoker.bind( this );
    actionInvokerBinded( httpContext, controllerName, actionName, function(err,result){
        res.status(200).send(result);
    });

}

function actionInvoker( httpContext,controllerName, actionName, callback ){

    var controller = this.controllers[controllerName];
    var action = controller[actionName];

    // authorizing
    var filterContext = {
        httpContext: httpContext,

        result: function(res){
            if(!res)
                return this.responseModel;
            this.stopExecutionOfFilters = true;
            this.responseModel = res;
        }
    };

    var self = this;
    var executeFilterBinded = executeFilters.bind(self);
    executeFilterBinded(filterContext,"authorizing", action,function() {

        if (filterContext.result())
            callback(null, filterContext.result());
        else
            executeFilterBinded(filterContext, "executing", action, function () {
                var handleCallback = function( err, res ){
                    if(err){
                        // error
                        filterContext.error = err;
                        executeFilterBinded(filterContext, "error", action, function (e) {
                            if (filterContext.result())
                                callback(null, filterContext.result());
                            else
                                callback(e || err);
                        });
                    }
                    else{
                        // executed
                        filterContext.result(res);
                        filterContext.stopExecutionOfFilters = false;
                        executeFilterBinded(filterContext, "executed", action, function (e) {
                            if(e)
                                callback(e);
                            else
                                callback(null,filterContext.result());
                        });
                    }
                };

                if (!filterContext.result())
                    action.call(controller, 1, handleCallback.bind(self));
                else
                    callback(null, filterContext.result());
            })
    })


};

function executeFilters( context, type, action, done){
    var index = 0;
    if(this.filters.length==0){
        done.bind(this)(null,null);
    }
    else
        async.doUntil(
            function executeFilter( next ){
                var Filter = this.filters[index];
                context.annotation = action.getType().annotations[Filter.filterName];
                var filter = new Filter(context);
                if(!filter.hasOwnProperty("value"))
                    filter.value = context.annotation.value;

                if(filter[type]) {
                    filter[type](context, function (err) {
                        index++;
                        next(err)
                    });
                }
                else
                    next();

            }.bind(this),

            function shouldStop(){
                return !(this.filters.length > 0 && this.filters.length > index && !context.stopExecutionOfFilters);
            }.bind(this),

            done.bind(this)
        );
}

MvcApplication.prototype = {

    filter: function( name, config ){
        if(typeof name != 'string' || !name)
            throw new Error("invalid parameter value: name");

        if(this.filterNames.hasOwnProperty(name))
            throw new Error("filter is already defined: " + name);

        var Filter = Weaver.type(config);
        Filter.filterName = name;

        this.filters.push( Filter );
        this.filterNames.push(name);

        Weaver.annotation(name, {
            ctor: function( context ){
                this.value = context.value;
                this.ctorInfo = context.ctorInfo;
                this.methodInfo = context.methodInfo;
            }
        });
    },

    controller: function( name, config){
        this.controllers[name] = this.container.instantiate(Weaver.type(config)); // every controller is cached as a singlton
    },

    getContainer: function(){
        return this.container;
    },

    actionInvoker: actionInvoker
};


