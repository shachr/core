/**
 * Created by shachar on 5/22/15.
 */
var Weaver              = require("./");
var async               = require("async");
var MvcApplication      = require("../application");



var startDate1 = new Date();
function a(key,callback){
    callback(null, {key: key, value: "shachar"});
}
a(1, function(err,res){
    console.log("native: " + (new Date()-startDate1));
});

var app = new MvcApplication( {
    use: function(){}
} );

var container = app.getContainer();
Weaver.annotation("cache", {
    ctor: function( context ){
        this.key = context.value;
        this.ctorInfo = context.ctorInfo;
        this.methodInfo = context.methodInfo;
        this.usersCache = {};
    },

    createKey: function( context ){

        var cacheAnnoValue = context.methodInfo.annotations.cache.key;
        var cacheKey = [context.methodInfo.typeInfo.hashcode , context.methodInfo.name ];
        cacheAnnoValue = cacheAnnoValue instanceof Array ? cacheAnnoValue : [cacheAnnoValue];
        cacheAnnoValue.forEach(function(key){
            var param = context.methodInfo.parameters[key];
            if(!param)
                console.warn("param not found:" + key);
            else
                cacheKey.push( param.getValue(context.args) )
        });

        return cacheKey.join("/");

    },

    onEntry: function( context, next ){
        var cacheKey = this.createKey( context );
        var user = this.usersCache[cacheKey];

        if(user)
            return next(null, user);

        return next();
    },

    onExit: function( context, next  ){
        var cacheKey = this.createKey( context );
        this.usersCache[cacheKey] = context.result;
        next();
    }
});
Weaver.annotation("errorHandle", {
    ctor: function(){},
    onError: function( context, next ){
        next();
    }
});
Weaver.annotation("inject", {

    ctor: function( context ){
        var typeInfo = (context.methodInfo || context.ctorInfo).typeInfo;
        for(var key in context.value){
            if(context.value.hasOwnProperty(key)) {
                var path = context.value[key];
                if (path.indexOf(".") == 0)
                    context.value[key] = require("path").resolve(require("path").dirname(typeInfo.module.filename), path);
            }
            this.keys = context.value;
        }
    }
    //onConstructorEntry: function( context, next ){
    //    var inject = context.ctorInfo.annotations.inject;
    //    var params = context.ctorInfo.parameters;
    //
    //    for(var key in inject){
    //        if(!inject.hasOwnProperty(key)) continue;
    //
    //        var param = params[key];
    //        var path = inject[key];
    //        var module = require(path);
    //
    //        context.args.splice(param.getIndex(), 1, module );
    //    }
    //
    //    next();
    //}
});

container.bind("./logger").to(console);
container.require("./usersDal").inSingletonScope();



app.filter("verb", {


    authorizing: function( filterContext, next ){
        next();
    },

    executing: function( filterContext, next ){
        //if( this.value.indexOf( "DELETE" ) == -1 )
        //    filterContext.result({ errorCode: 405001, errorMessage: "Not implemented" });

        next();
    },

    executed: function( filterContext, next ){

        var res = filterContext.result() || {};
        res.test = 1;
        filterContext.result(res);
        //filterContext.result({ errorCode: 405001, errorMessage: "Not implemented" });
        next()
    },

    error: function( filterContext, next ){

        filterContext.result({ errorCode: 500001, errorMessage: "Unhandeled error" });
        next();
    }
});

app.filter("verb1", {


    authorizing: function( filterContext, next ){
        next();
    },

    executing: function( filterContext, next ){
        //if( this.value.indexOf( "DELETE" ) == -1 )
        //    filterContext.result({ errorCode: 405001, errorMessage: "Not implemented" });

        next();
    },

    executed: function( filterContext, next ){

        var res = filterContext.result() || {};
        res.test = 1;
        filterContext.result(res);
        //filterContext.result({ errorCode: 405001, errorMessage: "Not implemented" });
        next()
    },

    error: function( filterContext, next ){

        filterContext.result({ errorCode: 500001, errorMessage: "Unhandeled error 1" });
        next();
    }
});

app.controller("home", {

    ctor: {
        $inject: {usersDal: "./usersDal"},
        method: function(usersDal){
            this.usersDal = usersDal;
        }
    },

   getUser: {
       $cache: ["userId"],
       $verb: ["GET","POST"],
       $verb1: ["GET","POST"],
       method: function( userId, next ){
            this.usersDal.getUser(userId, next)
       }
   }
});

var startDate2 = new Date();
app.actionInvoker({},"home", "getUser", function( err, res){
    console.log("controller: " + (new Date()-startDate2));
    console.log(res);
});


var startDate3 = new Date();
var UsersDal = require("./usersDal");
new UsersDal().getUser(1, function(){
    console.log("weaver: " + (new Date()-startDate3));
    console.log("overall: " + (new Date()-startDate1));
});





//usersDal.getUser(1, function( err, user ){
//    usersDal.getUser(1, function( err, user ){
//        this.logger.log(err, user)
//    });
//});


