/**
 * Created by shachar on 5/22/15.
 */

var _               = require("lodash"); // loadsh taskes around 40-50 ms to load!!!
var uuid            = require("node-uuid");
var registeredAnnotations = {};

var TypeInfo = function(){};
var CtorInfo = function(){};
var PropertyInfo = function(){};
var MethodInfo = function(){};
var Parameter = function (name, inx){ this.name = name; this.index = inx; };
Parameter.prototype = {
    getName: function(){
        return this.name;
    },
    getIndex: function(){
        return this.index;
    },
    getValue: function( args ){
        return args[this.index];
    }
};

Weaver = {};
Weaver.annotation = function( name, anno ){
      // global annotaions

      // OnConstructorEntry
      // onEntry
      // onError
      // onExit
      registeredAnnotations[name] = anno;
};

Weaver.type = function( config, parent ){
    if( !config.ctor )
        config.ctor = { method: function(){ } };

    // ctor
    var Type = Weaver.ctor(config.ctor, parent);

    //methods
    for( var key in config )
        if(config.hasOwnProperty(key) && key != 'ctor')
            Type.prototype[key] = Weaver.method( Type.constructor.getType(), key, config[key] );

    Type.prototype.getType = function(){
        return Type;
    };
    return Type;
};

Weaver.ctor = function( config, parent ){
    if(typeof config == 'function')
        config = {method: config };

    if(!config.method ) config.method = function(){};
    var typeInfo = new TypeInfo();
    typeInfo.module = module.parent;
    typeInfo.hashcode = uuid.v1();
    var ctorInfo = typeInfo.ctorInfo = new CtorInfo();
    typeInfo.ctorInfo.typeInfo = typeInfo;
    ctorInfo.method = config.method;

    delete config.method;
    ctorInfo.annotations = {};
    for(var key in config){

        if(config.hasOwnProperty(key) && key.indexOf("$") == 0) {
            var annoName = key.substring(1);
            var annoValue = config[key];
            var anno = registeredAnnotations[ annoName];
            var ctor = anno.ctor;
            if(ctor) {
                delete anno.ctor;
                ctor.prototype = anno;
                ctorInfo.annotations[ annoName ] = new ctor({ctorInfo: ctorInfo, value: annoValue});
            }
        }
    }


    // parameters
    var parameters = {};
    var paramNames = getParamNames(ctorInfo.method);
    paramNames.forEach( function(param, inx){
        parameters[param] = new Parameter(param, inx);
    });
    ctorInfo.parameters= parameters;

    var Type = function (a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p) {

        if (!(this instanceof Type))
            return new Type(a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p);

        // call parent
        if(parent)
            parent.call(this);

        // aspects
        var args = Array.prototype.slice.call(arguments,0);
        var self = this;
        executeAOP("onConstructorEntry", ctorInfo,
            {
                args: args
            },
            function( err ){
                if(err)
                    throw err;

                // call ctor
                if(ctorInfo.method)
                    ctorInfo.method.apply(self, args);
            }
        );

        return this;
    };

    Type.constructor.getType = function(){ return this; }.bind(typeInfo);

    return Type;
};

Weaver.method =  function( typeInfo, name, config ){

    if(typeof config == 'function')
        config = { method: config };

    var methodInfo = new MethodInfo();
    methodInfo.typeInfo = typeInfo;
    methodInfo.name = name;
    methodInfo.method = config.method;
    methodInfo.annotations= {};
    methodInfo.hashcode = uuid.v1();

    // annotations
    delete config.method;
    for(var key in config){
        if(config.hasOwnProperty(key) && key.indexOf("$") == 0) {
            var annoName = key.substring(1);
            var annoValue = config[key];

            var anno = registeredAnnotations[ annoName];

            var ctor = anno.ctor;
            if(ctor) {
                delete anno.ctor;
                ctor.prototype = anno;
                methodInfo.annotations[ annoName ] = new ctor({methodInfo: methodInfo, value: annoValue});
            }
        }
    }


    // parameters
    methodInfo.parameters = {};
    var paramNames = getParamNames(methodInfo.method);
    paramNames.forEach( function(param, inx){
        methodInfo.parameters[param] = new Parameter(param, inx);
    });
    return createMethod(methodInfo);
};

Weaver.property = function( typeiInfo, name , config ){

    var propertyInfo = new PropertyInfo();
    propertyInfo.typeInfo = typeInfo;
    propertyInfo.name = name;
    propertyInfo.hashcode = uuid.v1();

    // annotations
    var getterConfig = {};
    var setterConfig = {};
    var annotations = {};
    for(var key in config)
    {
        if (config.hasOwnProperty(key)) {

            if (key.indexOf("$") == 0) {
                var annoName = key.substring(1);
                var annoValue = config[key];

                var anno = registeredAnnotations[annoName];

                var ctor = anno.ctor;
                if (ctor) {
                    delete anno.ctor;
                    ctor.prototype = anno;
                    propertyInfo.annotations[annoName] = new ctor({methodInfo: propertyInfo, value: annoValue});
                }
            }
            else if( key == 'get' )
            {
                getterConfig.method = config[key];
                getterConfig.annotations = annotations;
                annotations = {};
            }
            else if( key == 'set ')
            {
                setterConfig.method = config[key];
                setterConfig.annotations = annotations;
                annotations = {};
            }

            propertyInfo.autoField = "__autofield__" + name;
            var isAutoGetter    = typeof getterConfig.method == 'boolean';
            var isAutoSetter    = typeof setterConfig.method == 'boolean';
            var isManualGetter  = typeof getterConfig.method == 'function';
            var isManualSetter  = typeof setterConfig.method == 'function';

            if(isAutoGetter || isAutoSetter){
                if(isManualGetter || isManualSetter)
                    throw new TypeError("Auto property can not be declared with a function");

                if(isAutoGetter) {
                    getterConfig.method = function () {
                        return this[propertyInfo.autoField];
                    };
                }

                if(isAutoSetter){
                    setterConfig.method = function (value) {
                        return this[propertyInfo.autoField] = value;
                    };
                }

                if(isAutoGetter && isAutoSetter){
                    propertyInfo.method = function(){
                        if(arguments.length == 0)
                            return this[propertyInfo.autoField];
                        return this[propertyInfo.autoField] = value;
                    };
                }
            }


            propertyInfo.setMethod = new MethodInfo(typeInfo, name, setterConfig);
            propertyInfo.getMethod = new MethodInfo(typeInfo, name, getterConfig);
            propertyInfo.method = function(){
                if(propertyInfo.getMethod && arguments.length == 0)
                    return propertyInfo.getMethod.call(this);
                else if(propertyInfo.setMethod && arguments.length > 1)
                    return propertyInfo.setMethod.call(this, arguments[0]);
                return undefined;
            }

        }
    }


    // parameters
    propertyInfo.parameters = {};
    if(propertyInfo.setMethod)
        propertyInfo.parameters["value"] = new Parameter("value", 0);


    return createMethod(propertyInfo);
};
module.exports = Weaver;


function getParamNames(fn) {
    var funStr = fn.toString();
    return funStr.slice(funStr.indexOf('(') + 1, funStr.indexOf(')')).match(/([^\s,]+)/g) || [];
}

function createMethod( methodInfo ){


    var fn = function(){
        var self = this;
        var args = Array.prototype.slice.call(arguments,0);
        var callback = args[args.length-1]; // last parameter expected to be a callback

        args[args.length-1] = function(err, res ){
            if(err){
                //execute error
                executeAOP("onError", methodInfo,
                    {
                        error: err,
                        args: args
                    },
                    function( err,res ){
                        return callback(err, res);
                    }
                );
            }
            else {

                //execute exit
                executeAOP("onExit", methodInfo,
                    {
                        result: res,
                        args: args
                    },
                    function( err,res ){
                        return callback(err, res);
                    }
                );
            }
        };

        //execute entry
        executeAOP("onEntry", methodInfo,
            {
                args: args
            },
            function( err,res ){
                if(err)
                    return callback(err);
                if(res)
                    return callback(null, res);

                // execute method
                methodInfo.method.apply(self, args);
            }
        );
    };

    fn.getType = function(){ return this; }.bind(methodInfo);
    return fn;
}

function executeAOP( aspectMethodName, info, args, callback ){

    var annoKeys = null;

    var typeInfo = undefined;
    var ctorInfo = undefined;
    var methodInfo = undefined;

    if(info instanceof TypeInfo) {
        typeInfo = info;
    }
    else if(info instanceof MethodInfo) {
        methodInfo = info;
    }
    else if(info instanceof CtorInfo) {
        ctorInfo = info;
    }

    annoKeys = Object.keys(registeredAnnotations);
    if(!annoKeys.length) return callback(args.error, args.result);

    var index = 0;
    next();

    function next () {
        if(index >= annoKeys.length)
            return callback(args.error, args.result);

        var annoKey = annoKeys[index];
        var annotation = registeredAnnotations[annoKey];

        if(!annotation) {
            index++;
            return next();
        }

        annotation = (methodInfo||ctorInfo).annotations[annoKey];
        if(!annotation){
            index++;
            return next();
        }

        var aopMethod = annotation[aspectMethodName];
        if(!aopMethod) {
            index++;
            return next();
        }

        aopMethod.call(annotation,
            {
                ctorInfo: ctorInfo,
                typeInfo: typeInfo,
                methodInfo: methodInfo,
                error: args.error,
                result: args.result,
                args: args.args
            },
            function(err, res )
            {
                if(err)
                    return callback(err);

                if (res)
                    return callback(null, res);

                index++;
                return next();
            }
        );
    }
}