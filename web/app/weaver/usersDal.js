/**
 * Created by shachar on 5/23/15.
 */
var Weaver              = require("./");
var UsersDal = module.exports = Weaver.type({
    ctor: {
        $inject: {logger: "./logger"},
        method: function (logger) {
            this.logger = logger;
        }
    },

    getUser: {
        $cache: ["key"],
        method: function (key, callback) {
            callback(new Error("database error"), {key: key, value: "shachar"});
        }
    }
});