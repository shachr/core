/**
 * Created by shachar on 5/20/15.
 */
var util                = require("util");
var EventEmitter        = require('events').EventEmitter;

var Controller = module.exports = function(name, config){
    this.name = name;
    this.config = config;
};

Controller.prototype = {
    config: function( ){
        return this.config;
    }
};

//util.inherits(Controller, EventEmitter);

/*
    Usage:


-- url by convention:       {version/{controller}/{action}
-- schema by convention:    /schemas/{controller}.{actions}.json

-- code:
app.controller("home", {

    $inject: ["../services/logger"],
    ctor: function( logger ){
        this.logger = logger;
    },

    $errorHandle: function( context, err, next){},
    actions: {

        $verb: ["GET"], // verb annotation
        $authorization: true, // authorization annotation
        $requestModel: { schema: "home/index" }, // model annotation
        $actionName: "index",
        $errorHandle: function(){},
        $cache: function(){}, // custom annotations ( point cuts / AOP ? )
        index: function( context, requestModel ){

        },


        $verb: ["POST"],
        save: function(){

        }
    }
});


 */

