/**
 * Created by shachar on 3/6/15.
 */
var path = require("path");
var jp = require("jsonpath");
var extendify = require('extendify');
var extend = extendify({ inPlace:false, arrays: 'replace' });
var configurationManager = require("../configurationManager");

var cwd = process.cwd();
var appSettings = configurationManager.getSection("appSettings", {}) || {};
appSettings.config = appSettings.config || {};
appSettings.config.includes = !appSettings.config.includes ? [] : appSettings.config.includes;

var opt = {};
var config = {};
var includes = [];
var initialized = false;
var phPattern = /\%([^%]+)\%/;
var defaults = {
    variables: {
        env: process.env.NODE_ENV
    },

    envs: [process.env.NODE_ENV]
};

defaults = extend(defaults, appSettings.config || {});

module.exports = {
    init: function( options ){
        // ensure initialization hapens only once!
        if( initialized )return;
        initialized = true;

        // extend options parameter with the defaults
        opt = options = extend.call(null,defaults, options || {});

        // apply merge logic to the includes
        appSettings.config.includes.forEach(function(include)
        {
            if(!include)return;
            if(include.add)
                includes.push( include.add );
            else if(include.remove) {
                var inx = includes.indexOf(include.remove);
                if(inx>-1)
                    includes.splice(inx,0);
            }
            else if(include.clear)
                includes = []
        });

        // load includes and settings
        if(!options.envs || options.envs.length == 0)
            options.envs = [options.variables.env];

        options.envs.forEach( function(env)
        {
            console.log("loading appSettings for env: " + env);
            var params = [{},appSettings.settings];
            includes.forEach( function(include){
                include = include.replace(phPattern, function( match, grp){
                    if(grp == "env")
                        return env;

                    var value = options.variables[grp];
                    if(value)
                        return value;
                    else
                        return match;
                });

                if(include.indexOf(".") == 0)
                    include = path.resolve(cwd,include);

                try {
                    var mdl = require(include);
                    params.push( mdl );
                }catch( e ){
                    console.warn( "[appSettings] cannot load module: " + include );
                }
            });

            // update the global config obect
            config[env] = extend.apply(null, params);
        });

    },

    /**
     * Given a json path returns the configuration element
     * @param path jsonpath to the object in the config
     * @defValue path default value
     * @env environemnt optional
     */
    getSync: function( path , defValue, env ){
        this.init();

        if( env && opt.envs.indexOf(env)==-1 )
            return undefined;

        env = env || opt.variables.env;

        var res = jp.query( config[env] || {}, path );
        if(res && res.length==1)
            return res[0];
        return defValue;
    },

    /**
     * Given a path returns paths of all sub elements.
     * @param path jsonpath to the object in the config
     */
    getPathsSync: function( path ){
        this.init();
        return jp.paths(config, path);
    }
}