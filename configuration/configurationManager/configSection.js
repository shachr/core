/**
 * Created by shachar on 3/7/15.
 */

var ConfigSection = function( config ){
    this.$ = config.$;
    this.ops = {
        $set: function(changes){
            if( !(changes instanceof Array) && typeof changes != 'object')
                throw new Error("type of $addToSet has to be Object");

            for(var key in changes){
                if(!changes.hasOwnProperty(key))continue;
                this[key] = changes[key];
            }
        },
        $addToSet: function(changes){
            if( !(changes instanceof Array) && typeof changes != 'object')
                throw new Error("type of $addToSet has to be Object");

            var self = this;
            for(var key in changes){
                if(!changes.hasOwnProperty(key))continue;
                var arr = changes[key];
                var val = this[key];
                arr.forEach( function(item){
                    if(val.indexOf(item) == -1)
                        val.push(item);
                });
            }
        },

        $unset: function(changes){
            if( !(changes instanceof Array) && typeof changes != 'object')
                throw new Error("type of $unset has to be Object");

            for(var key in changes){
                if(!changes.hasOwnProperty(key))continue;
                if(changes[key])
                    delete this[key];
            }
        }
    };
    this.data = {};

    // copy values
    for(var key in config){
        if(config.hasOwnProperty(key) && key != "$"){
            this.data[key] = config[key];
        }
    }

};

ConfigSection.prototype.applyChanges = function( changes ){
    iterate.call(this, this.data, changes);

    function iterate(data ,changes){
         for(var key in changes){
             if(!changes.hasOwnProperty(key))continue;

             var exists = undefined;
             if(key.indexOf("$") == 0){
                 // is operation
                 var op = this.ops[key];
                 var opValue = changes[key];

                 if(!op)
                     throw new Error("op "+ key + " is not supported");

                 op.call(data, opValue);

             }
             else {
                 // keep iterating
                 var change = changes[key];
                 if(!data.hasOwnProperty(key))
                     data[key] = {}; //todo: check schema for type?

                 exists = data[key];

                 if (!( change instanceof Array) && typeof change == 'object') {
                     // keep iterating
                     iterate.call(this, exists, change);
                 }
                 else{
                     // just set
                     data[key] = change;
                 }
             }

         }
     }
};

module.exports = ConfigSection;