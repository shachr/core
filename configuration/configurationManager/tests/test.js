/**
 * Created by shachar on 5/2/15.
 */
var JaySchema = require('jayschema');
var extendify = require('extendify');
var config = require("./web.json");

var extend = extendify({ inPlace:false, arrays: 'concat' });
var configSections = config.configSections;
var master = config.master;
var child = config.child;


var js = new JaySchema();
var data = {};
var configSectionsIds = {};
function loadConfigSections( config, registerNames ){
    if(arguments.length == 1 ) registerNames = true;

    for(var name in config){
        if(!config.hasOwnProperty(name))return;
        var configSection = config[name];
        js.register(configSection, configSection.id);

        if(registerNames && !configSection._ignore) {
            configSectionsIds[name] = {id: configSection.id};
        }

    }
}

function loadConfig( config ){

    // load config sections
    loadConfigSections(config.configSections);

    // merge with master
    config = extend( master, config );

    // read config
    Object.keys(configSectionsIds).forEach( function( id ){
       var ref = configSectionsIds[id];
       if(config.hasOwnProperty(id)){
           var node = config[id];
           var validationResults = js.validate(node, ref.id);
           if(!validationResults || !validationResults.length) {
               data[id] = node;
           }
           else {

               var err = new Error("config sections not valid");
               err.validationResults = validationResults;
           }
       }
    });
}

loadConfigSections(master.configSections);
loadConfig(child);

module.exports = {
    getSection: function(name){
        return data[name];
    }
}



// synchronous…
//console.log('synchronous result:', js.validate(instance, schema));

// …or async
//js.validate(instance, schema, function(errs) {
//    if (errs) { console.error(errs); }
//    else { console.log('async validation OK!'); }
//});