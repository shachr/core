/**
 * Created by shachar on 3/7/15.
 */

var ConfigSection = require("../configSection");

var parent = {
    appSettings: {
        // only the declaring web.json can declare this section
        $: {
            module: {
                // to ensure that we pass this section to the allowed module!
                filename: ''
            },

            schema: {
                filename: '' // optional, json-schema ?
            },

            inherit: {
                allow: true, // default is true, meaning chuldren can add/remove/replace values
                sealed: { //optionaly seal an object from being overrided
                    'settings.database.users': true
                }
            }
        },

        // initial values
        includes: [],
        settings: {
            database: {
                leads: {
                    host: '',
                    port: 0,
                    obj: {
                        name: ""
                    },
                    array: [],
                    rem: "to remove"
                },
                users: {

                }
            }
        }
    }
};


var child = {
    appSettings: {
        //$ is reserved key for operations
        settings:{
            database: {
                leads: {
                    $unset: {
                        rem: true
                    },
                    $set: {
                        host:"localhost",
                        port: 3306
                    },
                    $addToSet: { array: [1] },

                    obj: {
                        $set: { name: "shachar", age: 31 }
                    }
                },
                users: {
                    host: 'aaa.com'
                }
            }
        },

        $addToSet: {
            includes: [ // will work only if includes was set t array
                "/root/config/*.json"
            ]
        }
    }
};


var configSection = new ConfigSection( parent.appSettings );
configSection.applyChanges( child.appSettings );

console.log("start");

console.log(JSON.stringify(configSection.data,null,4));

console.log("end");
