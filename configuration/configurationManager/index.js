/**
 * Created by shachar on 3/7/15.
 */
console.log("node env: " + process.env.NODE_ENV);
var fs = require('fs'),
    path = require('path');

var JaySchema = require('jayschema');
var extendify = require('extendify'); // https://github.com/bigShai/extendify
var master = require("./master.json");
var extend = extendify({ inPlace:false, arrays: 'merge' });


var js = new JaySchema();
var data = {};
var configSectionsIds = {};

var cwd = process.cwd();

var configSections = {};

var assert = {
    sectionExists: function(sectionName){
        var section = configSections[sectionName];
        if( !section ) throw new Error("section "+ sectionName +" doesn't exists");
        return section;
    },
    dirname: function(dirname, sections ){
        sections.forEach( function(sectionName){
            var section = assert.sectionExists(sectionName);
            var path = path.resolve(cwd, section.require);
            if(dirname != path) throw new Error("Permissions denied to access "+ sectionName + " section");
        })
    }
};

var configurationManagerReader = {

    initialize: function(){
        console.log("cwd: " + cwd);
        console.log("main: " + require.main.filename);

        try {
            configurationManagerReader.loadConfigSections(master.configSections);

            configurationManagerReader.tryInitializeDirectory(cwd);

            var dirname = path.dirname(require.main.filename);
            configurationManagerReader.tryInitializeDirectory(dirname);
        }catch(e){
            console.error(e);
        }
    },

    tryInitializeDirectory: function( dirname ){
        if(dirname == undefined)return;

        try {
            var parts = dirname.split(path.sep);

            var dir = "";
            for (var inx = 0; inx < parts.length; inx++) {
                dir += parts[inx] + path.sep;
                configurationManagerReader.tryReadConfig(dir);
            }
        }catch(e){
            console.error(e);
        }
    },

    tryRequire: function(dirname, env){
        var filename = env ? path.join(dirname, "web."+ env +".json") : path.join(dirname, "web.json");
        console.log("trying to load app config: " + filename);
        if (!fs.existsSync(filename)) return undefined; // if doesn't exists ignore.
        console.log("loading app config: " + filename);
        try {
            return require(filename);
        }catch(e){
            console.error(e);
            return undefined;
        }
    },

    tryReadConfig: function(dirname){

        // read web.json
        var webJson = configurationManagerReader.tryRequire(dirname);
        if(webJson)
            configurationManagerReader.readConfig( webJson );

        // consider web.%env%.json as an explicit override to web.json!
        var webEnvJson = configurationManagerReader.tryRequire(dirname,process.env.NODE_ENV);
        if(webEnvJson)
            configurationManagerReader.readConfig(webEnvJson);

    },

    loadConfigSections: function( config, registerNames ){
        if(arguments.length == 1 ) registerNames = true;

        for(var name in config){
            if(!config.hasOwnProperty(name))return;
            var configSection = config[name];
            js.register(configSection, configSection.id);

            if(registerNames) {
                configSectionsIds[name] = {id: configSection.id};
            }

        }
    },


    readConfig: function( config ){
        // load config sections
        configurationManagerReader.loadConfigSections(config.configSections);

        // merge with master
        config = extend( master, config );

        // read config
        Object.keys(configSectionsIds).forEach( function( id ){
            var ref = configSectionsIds[id];
            if(config.hasOwnProperty(id)){
                var node = config[id];
                var validationResults = js.validate(node, ref.id);

                if(!validationResults || !validationResults.length) {
                    data[id] = node;
                }
                else {

                    var err = new Error("config sections not valid " + id + ", " + validationResults[0].desc);
                    err.validationResults = validationResults;
                    throw err;
                }
            }
        });
    }
};

configurationManagerReader.initialize();


module.exports = {
    getSection: function(name){
        return data[name];
    }
};

