/**
 * Created by shachar on 6/9/15.
 */
var util                  = require("util");
var ActionFilter          = require("../../../soa/mvc/action-filter");

var ExecutedFilter = module.exports = function ExecutedFilter(){
    ActionFilter.call(this);
    this.on("executed", function( filterContext, next ){
        var res = filterContext.result();
        res.executed = true;
        filterContext.result( res );
    });
}
util.inherits(ExecutedFilter,ActionFilter);