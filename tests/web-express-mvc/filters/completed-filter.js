/**
 * Created by shachar on 6/9/15.
 */
var util                  = require("util");
var ActionFilter          = require("../../../soa/mvc/action-filter");

var CompletedFilter = module.exports = function CompletedFilter(){
    ActionFilter.call(this);
    this.on("completed", function( filterContext, next ){
        filterContext.result({ completed: true});
    });
}
util.inherits(CompletedFilter,ActionFilter);