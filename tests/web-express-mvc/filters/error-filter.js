/**
 * Created by shachar on 6/9/15.
 */
var util                  = require("util");
var ActionFilter          = require("../../../soa/mvc/action-filter");

var FixedResultFilter = module.exports = function FixedResultFilter(){
    ActionFilter.call(this);
    this.on("executing", function( filterContext, next ){
        filterContext.result({ error: true });
    });
}
util.inherits(FixedResultFilter,ActionFilter);