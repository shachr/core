/**
 * Created by shachar on 6/8/15.
 */
//var JaySchema           = require("JaySchema");
var validator = require('is-my-json-valid');

module.exports = {
    $setup: function(){
        this.on("model-binding", modelBindingWithFilter);
    }
};

function modelBindingWithFilter( eventArgs ){
    var modelConfig = eventArgs.args.modelConfig;
    var context = eventArgs.args.context;

    var validate = validator.filter(modelConfig._schema, { greedy: true, verbose: true});//, filter:true
    var isValid = validate(context.request);

    var validationErrors = [];
    if(!isValid) {
        var filteredOut = [];
        validate.errors.forEach(function (err) {
            if (err.message != 'has additional properties')
                validationErrors.push(err);
            else
                filteredOut.push(err);
        });
    }

    if(validationErrors.length)
        eventArgs.error( validationErrors.errors );
    else
        eventArgs.result( context.request );
}

function modelBinding( eventArgs ){

    var modelConfig = eventArgs.args.modelConfig;
    var context = eventArgs.args.context;

    if(!this._schema.isRegistered(modelConfig._schema.id))
        this._schema.register(modelConfig._schema,modelConfig._schema.id);

    var validationResult = this._schema.validate(context.request, modelConfig._schema.id);
    if(validationResult && validationResult.length ){
        var propertiesToDelete = [];
        var finalValidationErrors = [];
        validationResult.forEach( function(vr){

            if(vr.kind == 'ObjectValidationError' && vr.constraintName == 'additionalProperties'){
                console.log(vr)
                var prop = vr.testedValue;
                if(vr.instanceContext.indexOf("#/")==0){

                    prop = vr.instanceContext.substring(2) + "." + prop;
                }
                propertiesToDelete.push(prop);
            }else{
                finalValidationErrors.push(vr);
            }
        });
        if(finalValidationErrors.length)
            return eventArgs.error(finalValidationErrors[0]);
        else{
            propertiesToDelete.forEach( function( prop ){
                console.log(prop);
                eval("delete context.request." + prop + ";");//todo: change to something more elegant
                //delete context.request[prop];
            })
        }
    }

    eventArgs.result(context.request);
}