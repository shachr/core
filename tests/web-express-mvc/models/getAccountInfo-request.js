/**
 * Created by shachar on 6/4/15.
 */
module.exports = function(){

  this.schema({
    id:"/api/accounts/getAccountInfo",
    additionalProperties: false,
    properties: {
      id: { type: "number" },
      age: { type: "number" },

      array: {
        type:"array",
        items:{
          type: "string"
        }
      },

      obj: {
        type: "object",
        additionalProperties: false,
        properties: {
          name: {type:"string"}
        }
      }
    },

    required: ["id"]

  })
};

