/**
 * Created by shachar on 6/4/15.
 */
var assert              = require("assert");
var ExecutingFilter     = require("../filters/executing-filter");
var ExecutedFilter      = require("../filters/executed-filter");
var ErrorFilter         = require("../filters/error-filter");
var CompletedFilter     = require("../filters/completed-filter");
module.exports = function(){
    this.name("account");
    this.getAccountInfo = this.action(function( model, next ){

        assert.equal(model.id, 1);
        assert.equal(model.notInSchema==undefined, true);

        next(null,{
            id: model.id,
            name: "Shachar"
        })
    }).model( require("../models/getAccountInfo-request") );


    this.executingAction = this.action( function( model, next ){
        next(null,{ notFromFilter: true })
    })
        .model( require("../models/getAccountInfo-request") )
        .filters([
            new ExecutingFilter()
        ]);

    this.executedFilter = this.action( function( model, next ){
        next(null,{ notFromFilter: true })
    })
        .model( require("../models/getAccountInfo-request") )
        .filters([
            new ExecutedFilter()
        ]);

    this.errorFilter = this.action( function( model, next ){
        next(new Error("aaa"));
    })
        .model( require("../models/getAccountInfo-request") )
        .filters([
            new ErrorFilter()
        ]);

    this.completedFilter = this.action( function( model, next ){

    })
        .model( require("../models/getAccountInfo-request") )
        .filters([
            new CompletedFilter()
        ]);

    this.mixedFilters = this.action( function( model, next ){
        next(null,{ notFromFilter: true })
    })
        .model( require("../models/getAccountInfo-request") )
        .filters([
            new ExecutingFilter(),
            new ExecutedFilter(),
            new ErrorFilter(),
            new CompletedFilter()
        ]);

    this.timeout = this.action(function(){

    })
        .model( require("../models/getAccountInfo-request") );


    this.on("action-not-found",function( envalope, next ){
        assert.equal(envalope.request.path, "/admin/account.noAction");

        next(null,{
            errorCode: 404000,
            errorMessage: "Action not found"
        })
    });
};
