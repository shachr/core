/**
 * Created by shachar on 6/4/15.
 */
var assert              = require("assert");

module.exports = function(){
    this.name("account1");
    this.getAccountInfo = this.action(function( model, next ){

      assert.equal(model.id, 1);
      assert.equal(model.notInSchema==undefined, true);

      next(null,{
          id: model.id,
          name: "Shachar"
      })
    }).model( require("../models/getAccountInfo-request") );

};
