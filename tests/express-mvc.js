/**
 * Created by shachar on 6/2/15.
 */
var mvc                         = require("../soa/mvc");
var assert                      = require("assert");
var RouteNotFoundError          = require("../soa/mvc/errors/route-not-found-error");
var ControllerNotFoundError     = require("../soa/mvc/errors/controller-not-found-error");
var ActionNotFoundError         = require("../soa/mvc/errors/action-not-found-error");
var TimeoutError                = require("../soa/mvc/errors/timeout-error");

describe('express-mvc', function(){
    var app = mvc();
    app.use("callback-timeout",1000);
    app.getRoutePatterns().use("uriSegment","[^\\.\\/]+");

    // root
    var defArea = app.area("/")
        .controller( "./web-express-mvc/controllers/", "{name}-controller" );

    defArea.route("{controller:uriSegment}.{action:uriSegment}").defaults({ controller: "home", action: "index"});

    // admin
    var adminArea = app.area("/admin")
        .controller( "./web-express-mvc/controllers/", "{name}-controller" );

    adminArea.route("{controller:uriSegment}.{action:uriSegment}").defaults({ controller: "home", action: "index"});

    assert.equal(defArea._controllers.length!=0,true);
    assert.equal(adminArea._controllers.length!=0,true);

    describe('route tests', function() {

        it('app is an instance', function () {
            assert.equal(true, app != undefined);
        });

        it('app match default route', function () {
            var route = app._callExecuter.findRoute(("/account.getAccountInfo"));
            assert.equal(true, route != null);
            assert.equal(route.getParams().area, "/");
            assert.equal(route.getParams().controller, "account");
            assert.equal(route.getParams().action, "getAccountInfo");
        });

        it('app match default route use defaults params', function () {
            var route = app._callExecuter.findRoute("/");
            assert.equal(true, route != false);
            assert.equal(route.getParams().area, "/");
            assert.equal(route.getParams().controller, "home");
            assert.equal(route.getParams().action, "index");
        });

        it('app match admin route', function () {
            var route = app._callExecuter.findRoute(("/admin/account.getAccountInfo"));
            assert.equal(true, route != null);
            assert.equal(route.getParams().area, "/admin/");
            assert.equal(route.getParams().controller, "account");
            assert.equal(route.getParams().action, "getAccountInfo");
        });

        it('app match admin route use defaults params', function () {
            var route = app._callExecuter.findRoute("/admin/"); // area has precedence over route match
            assert.equal(true, route != null);
            assert.equal(route.getParams().area, "/admin/");
            assert.equal(route.getParams().controller, "home");
            assert.equal(route.getParams().action, "index");

        });
    });

    describe('controller tests', function(){
        it("find route and controller", function(){
            var route = app._callExecuter.findRoute("/account");
            assert.equal(true, route != null);
            assert.equal(route.getParams().area, "/");
            assert.equal(route.getParams().controller, "account");
            assert.equal(route.getParams().action, "index");

            var area = route.getArea();
            var controllerName = route.getParams().controller;
            var controller = area.findController( controllerName );
            assert.equal(controllerName, "account");
            assert.equal(controller!=null, true);
        })
    });

    describe('action tests', function(){

        it("find route / controller / action", function() {
            var route = app._callExecuter.findRoute("/account.getAccountInfo");
            assert.equal(true, route != null);
            assert.equal(route.getParams().area, "/");
            assert.equal(route.getParams().controller, "account");
            assert.equal(route.getParams().action, "getAccountInfo");

            var controller = route.options().area.findController(route.getParams().controller);
            assert.equal(controller != null, true);

            var action = controller[route.getParams().action];
            assert.equal(action != null, true);
            action.invoke({id: 1}, function (err, accountInfo) {
                assert.equal(err, undefined);
                assert.equal(accountInfo.id, 1);
                assert.equal(accountInfo.name, "Shachar");
            });
        });

        it("execute action inside area", function() {
            app.execute({
                path: "/admin/account.getAccountInfo",
                params: { notInSchema: 1, id: 1}
            }, function (err, accountInfo) {
                assert.equal(err, undefined);
                assert.equal(accountInfo.id, 1);
                assert.equal(accountInfo.name, "Shachar");
            });
        });

        it("execute an unknown area/route", function() {

            app.execute({path: "/abc/account.getAccountInfo", params: { notInSchema: 1, id: 1}}, function (err) {
                assert.equal(err != null, true);
                assert.equal(err instanceof RouteNotFoundError, true);
            });
        });

        it("execute an unknown controller", function() {
            app.execute({path: "/admin/blah.getAccountInfo", params: { notInSchema: 1, id: 1}}, function (err) {
                assert.equal(err != null, true);
                assert.equal(err instanceof ControllerNotFoundError, true);
            });
        });


        it("execute an unknown action ", function() {
            app.execute({path: "/admin/account1.noAction", params: { notInSchema: 1, id: 1}}, function (err) {
                assert.equal(err != null, true);
                assert.equal(err instanceof ActionNotFoundError, true);
            });
        })

        it("execute an unknown action with action-not-found subscription", function() {
            app.execute({path: "/admin/account.noAction", params: {notInSchema: 1, id: 1}}, function (err, res) {
                assert.equal(err == null, true);
                assert.equal(res.errorCode, 404000);
            });
        });

        it("execute an action with executing filter", function() {
            app.execute({path: "/account.executingAction", params: {notInSchema: 1, id: 1}}, function (err, res) {
                assert.equal(err == null, true);
                assert.equal(res.executing, true);
            });
        })
        it("execute an action with executed filter", function() {
            app.execute({path: "/account.executedFilter", params: {notInSchema: 1, id: 1}}, function (err, res) {
                assert.equal(err == null, true);
                assert.equal(res.executed, true);
            });
        })

        it("execute an action with error filter", function() {
            app.execute({path: "/account.errorFilter", params: {notInSchema: 1, id: 1}}, function (err, res) {
                assert.equal(err == null, true);
                assert.equal(res.error, true);
            });
        });

        it("execute an action that doesn't trigger callback and expect timeout", function(){
            app.execute( { path: "/account.timeout", params: { notInSchema:1, id: 1 } }, function( err , res ){
                assert.equal(err!=null, true);
                assert.equal(err instanceof TimeoutError , true);
            });
        });

        it("execute an action with completed filter", function(){
            app.execute( { path: "/account.completedFilter", params: { notInSchema:1, id: 1 } }, function( err , res ){
                assert.equal(err==null, true);
                assert.equal(res.completed,true);
            });
        });


        it("execute an action with mixed filters", function(){
            app.execute( { path: "/account.mixedFilters", params: { notInSchema:1, id: 1 } }, function( err , res ){
                assert.equal(err==null, true);
                assert.equal(res.executing,true);
            });
        });


    });
});
