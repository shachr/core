/**
 * Created by shachar on 6/4/15.
 */
var fs = require("fs");
module.exports = {
    getFiles: function( path, pattern, callback){
        fs.readdir(path, function(err, res){
            if(!err) {
                var arr = [];
                res.forEach(function (dir) {
                    if(pattern.test( dir ))
                        arr.push(dir);
                });

                callback(null, arr);
            }else{
                callback(err);
            }
        })
    },

    getFilesSync: function( path, pattern){
        var res = fs.readdirSync(path);
        var arr = [];
        res.forEach(function (dir) {
            if(pattern.test( dir ))
                arr.push(dir);
        });
        return arr;
    }
};

