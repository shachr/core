/**
 * Created by shachar on 5/8/15.
 */
var Path        = require("path");
var uuid        = require("node-uuid");
var stackTrace  = require("stack-trace");

function createInstance( mdl, inject, typeInfo ){
    // if this module is specified as injectable
    var args = [];
    if( inject ) {
        if (inject instanceof Array && inject.length) {
            inject.forEach(function (m) {
                args.push(require(m));
            });

            //todo: return promise, pass callback??
            if (mdl.length < args.length) {
                var err = new TypeError("constructor doesn't accept at least " + args.length + " parameters.");
                err.code = 'DI_INJECTION_MISSING_PARAMETER';
                throw err;
            }
        }

        else if( typeInfo ) {

            var ctorInfo = typeInfo.ctorInfo;
            var parameters = ctorInfo.parameters;
            for(var key in inject){
                if(!inject.hasOwnProperty(key)) continue;

                var param = parameters[key];
                var path = inject[key];

                if(path.indexOf(".") == 0)
                    path = Path.resolve( Path.dirname( typeInfo.module.filename), path );

                var module = this.resolve( path );
                args.splice( param.getIndex(), 1, module );
            }
        }
    }

    if(mdl && (inject || typeInfo)) {
        var instance = mdl.apply(null, args);
        //instance.__proto__ = mdl.prototype;
        return instance;
    }

    return mdl;
}

var Container = function(){
    this.$moduleConfigs = {};
    this.$instances = {};
    this.containerId = uuid.v1();
};

Container.prototype = {
    instantiate: function( mdl ){
        var inject;
        var typeInfo;

        var isWeaved = mdl.constructor && typeof mdl.constructor.getType == 'function';

        if(mdl) {
            if (isWeaved) {
                typeInfo = mdl.constructor.getType();
                var annotations = typeInfo.ctorInfo.annotations;
                inject = annotations.inject && annotations.inject.keys ? annotations.inject.keys : undefined;
            }
            else if (mdl.$inject) {
                inject = mdl.$inject;
            }
        }

        if(inject || isWeaved)
            return (createInstance.call(this, mdl, inject, typeInfo));

        else
            return mdl;
    },
    bind: function( name ){

        var trace = stackTrace.get(arguments.callee.caller);
        var caller = trace[0];

        if(name.indexOf(".") == 0)
            name = Path.resolve( Path.dirname( caller.getFileName()), name );

        return this.$moduleConfigs[name] = new ModuleConfig(name);
    },

    require: function( name ){

        var trace = stackTrace.get(arguments.callee.caller);
        var caller = trace[0];

        if(name.indexOf(".") == 0)
            name = Path.resolve( Path.dirname( caller.getFileName()), name );

        var mdlConfig = this.$moduleConfigs[name] = new ModuleConfig(name);
        return mdlConfig.scope();
    },

    getModuleConfigs: function(){
        return this.$moduleConfigs;
    },

    resolve: function( name ){

        var moduleConfig = this.$moduleConfigs[name];

        if(moduleConfig){
            var to = moduleConfig.to() || require( name );
            var scope = moduleConfig.scope().getScope();
            var instance = undefined;
            switch( scope ){

                case lifeCycle.hybrid:
                    instance = new  (createInstance( to ) );
                    break;

                case lifeCycle.singleton:
                    instance = this.$instances[name];
                    if(!instance)
                        instance = this.$instances[name] = new  (createInstance( to ) );
                    break;

                case lifeCycle.domain:
                    if(!process.domain)
                        throw new Error("Execution path is not part of a domain");

                    var bag = process.domain["container_"+ this.containerId];
                    if(!bag)
                        bag = process.domain["container_"+ this.containerId] = {};

                    instance = bag[name];
                    if(!instance)
                        instance = bag[name] = new (createInstance( to ));

                    break;
                default:
                    instance = require(name);
                    break;
            }
            return instance;
        }

        return require(name);

    }
};

var lifeCycle = {
    singleton: 0,
    hybrid: 1,
    domain: 2
};


var ModuleConfig = function ( name ){
    this.$name = name;
    this.$to = undefined;
    this.$scope = new Scope();
};

ModuleConfig.prototype = {
    to: function( mdl ){
        if(arguments.length == 0)
            return this.$to;

        this.$to = mdl;
        return this.$scope;
    },
    scope: function( ){
        return this.$scope;
    }
};

var Scope = function( ){
    this.scope = lifeCycle.hybrid;
};

Scope.prototype = {

    inSingletonScope: function() {
        this.scope = lifeCycle.singleton;
        return this;
    },

    inHybridScope: function(){
        this.scope = lifeCycle.hybrid;
        return this;
    },

    inDomainScope: function(){
        this.scope = lifeCycle.domain;
        return this;
    },

    getScope: function( ){
        return this.scope;
    }
};

module.exports = Container;
