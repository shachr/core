/**
 * Created by shachar on 4/28/15.
 * todo: enable through config (app.json?)
 */
var Container = require('./container');
var pathModule = require('path');
var globalContainer = module.exports = new Container();
//require.cache = {}; // clear cache before we start?

module.constructor.prototype.require = function (path) {
    try {
        var dirname = pathModule.dirname(this.filename);
        if( path.indexOf("./") > -1 && path.indexOf("./") < 2 ){ // if starts with ./ or ../
            path = pathModule.resolve(dirname, path);
        }

        var mdl = globalContainer.resolve(path);
        if( !mdl ){
            mdl = this.constructor._load(path, this); //todo: suport DI here also?
            if(mdl)
                mdl = globalContainer.instantiate(mdl);
        }

        return mdl;

    } catch (err) {
        handleException( err, path );
    }
};

function handleException ( err , path){

    // if module not found, we have nothing to do, simply throw it back.
    if (err.code === 'MODULE_NOT_FOUND') {
        console.error(err);
    }
}



