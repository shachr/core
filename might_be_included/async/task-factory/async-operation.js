/**
 * Created by shachar on 6/28/15.
 */
module.exports = {

    make: function ( setup, execute, context, params, arg0,arg1,arg2,arg3 ) {

        var asyncConfig = { context: context || {}, params: params, then: {ok: null, err: null}};

        var externalArgs = arguments;
        var fn =  function(){
            if(typeof setup == 'function') setup();

            if(externalArgs.length == 3)
                execute( asyncConfig );

            else if(externalArgs.length == 4)
                execute( asyncConfig, arguments,arg0 );

            else if(externalArgs.length == 5)
                execute( asyncConfig, arguments,arg0, arg1 );

            else if(externalArgs.length == 6)
                execute( asyncConfig, arguments,arg0, arg1, arg2 );

            else if(externalArgs.length == 7)
                execute( asyncConfig, arguments,arg0, arg1, arg2, arg3 );

            else {
                var args = [asyncConfig,arguments];
                for(var inx=3;inx<externalArgs.length;inx++){
                    args.push( externalArgs[inx] );
                }
                execute.apply(null, args);
            }
        };

        fn.then = function ( handler ) {
            asyncConfig.then = handler;
            return fn;
        };

        fn.params = function( params ){
            asyncConfig.params = params;
            return fn;
        };

        return fn;
    }
};
