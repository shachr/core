/**
 * Created by shachar on 6/28/15.
 */
var parallel            = require("./parallel");

var Task = module.exports = function( context ){
    this._context = context;
};

Task.prototype = {
    params: function( params ){
        this._params = params;
        if(arguments.length)
            return this;
        return this._params;
    },

    parallel: function( tasks ){
        return parallel(this._context,this._params, tasks);
    }
};

