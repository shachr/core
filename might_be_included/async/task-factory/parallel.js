/**
 * Created by shachar on 6/28/15.
 */
var async           = require("async");
var AsyncOperation  = require("./async-operation");


var Parallel = module.exports = function( context, params, tasks ){
    return AsyncOperation.make(function( ){ }, execute, context, params, tasks);
};

function execute ( asyncConfig, args, tasks ){

    // ensure context is forced to each task.
    var dupTasks = {};
    for(var key in tasks)
        if (tasks.hasOwnProperty(key))
            dupTasks[key] = wrapTask( tasks[key] );

    var externalArgs = Array.prototype.splice.call(args,0);
    var callback = externalArgs.pop().bind( asyncConfig.context );


    function wrapTask( task ){

        return function(){
            var next = arguments[0];
            var context     = asyncConfig.context;
            var parameters  = getParamNames(task);
            var diParams    = findParams(externalArgs, parameters);

            if(diParams.length == 0)
                task = task.bind(context);

            else if(diParams.length == 1)
                task = task.bind(context, diParams[0]);

            else if(diParams.length == 2)
                task = task.bind(context, diParams[0], diParams[1]);

            else if(diParams.length == 3)
                task = task.bind(context, diParams[0] ,diParams[1], diParams[2]);

            else if(diParams.length == 4)
                task = task.bind(context, diParams[0] ,diParams[1], diParams[2], diParams[3]);

            else if(diParams.length == 5)
                task = task.bind(context, diParams[0] ,diParams[1], diParams[2], diParams[3], diParams[4]);

            else {
                var args = [context];
                for (var inx = 1; inx < diParams.length; inx++)
                    args.push(diParams[inx]);
                task = task.bind.call(task, args);
            }


            task( next );
        }
    }

    function getParamNames(fn) {
        var funStr = fn.toString();
        var names = funStr.slice(funStr.indexOf('(') + 1, funStr.indexOf(')')).match(/([^\s,]+)/g) || [];
        var params = {};
        names.forEach(function(name,inx){
            params[name] = { name: name, index: inx };
        });
        return params;
    }

    function findParams( args, parameters ){

        var outArgs = [];
        if(!asyncConfig.params || asyncConfig.params.length == 0)return outArgs;

        for(var parameterName in parameters){
            if( !parameters.hasOwnProperty(parameterName) ) continue;
            var parameter = parameters[parameterName];
            if(asyncConfig.params.hasOwnProperty( parameter.name ) )
                outArgs.push(args[parameter.index]);
        }

        return outArgs;
    }

    async.parallel( dupTasks, function( err,res )
    {
        if(this.then) {
            if (err)
                return this.then.call(this.context, err, null, callback);
            this.then.call(this.context, err, res, callback);
        } else {
            callback(err, res);
        }

    }.bind(asyncConfig) );
}




