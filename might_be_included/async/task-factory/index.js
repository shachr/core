/**
 * Created by shachar on 6/25/15.
 */
var Task                = require("./task");

var TaskFactory = module.exports = {
    create: function( context ){
        return new Task( context || {} );
    }
};





