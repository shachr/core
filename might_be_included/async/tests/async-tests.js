/**
 * Created by shachar on 6/28/15.
 */
var TaskFactory             = require("../task-factory/index");
var assert                  = require("assert");



// mongo db mock
var db = {
    findOne: function( query, cb ){
        process.nextTick( function() {
            cb(null, {_id: 1});
        });
    },
    find: function( query, cb ){
        process.nextTick( function() {
            cb(null, [{_id: 1},{_id: 2}]);
        });
    }
};


// tests
describe('async', function( endTest ){

    // setup
    var api = {};
    var task = TaskFactory.create(api);
    api.getProfile = task.params({id: 1}).parallel({
        portrait: function( id, next ){
            assert.equal(id,1);
            db.findOne({ _id: id }, next);
        },
        tgs: function( id, next ) {
            assert.equal(id,1);
            db.findOne({ _id: id }, next);
        }
    })
        .then(
        function( err, res, done ){
            assert.equal(err, undefined);
            assert.equal(res.portrait!= undefined, true);
            assert.equal(res.portrait._id, 1);

            assert.equal(res.tgs!= undefined, true);
            assert.equal(res.tgs._id, 1);
            done(null, res);
        }
    );

    api.getTrips = task.params({id: 1}).parallel({
        profile: function( id, next ){
            this.getProfile(id, next);
        },
        trips: function( id, next ) {
            assert.equal(id,1);
            db.find({ _id: id }, next);
        }
    })
        .then(
        function( err, res, done ){
            assert.equal(err, undefined);
            assert.equal(res.profile!= undefined, true);
            assert.equal(res.profile.portrait!= undefined, true);
            assert.equal(res.profile.portrait._id, 1);
            assert.equal(res.profile.tgs!= undefined, true);
            assert.equal(res.profile.tgs._id, 1);

            assert.equal(res.trips != undefined, true);
            assert.equal(res.trips instanceof Array, true);
            assert.equal(res.trips.length > 0, true);
            assert.equal(res.trips[0]._id, 1);

            done(null, res);
        }
    );


    // usage
    it("parallel", function( endTest){
        api.getProfile(1, function(err, res ){
            endTest()
        });
    });

    it("parallel nested", function(endTest){
        api.getTrips( 1, function(err, res ){

            assert.equal(res.profile != undefined ,true);
            assert.equal(res.trips.length > 0 ,true);
            endTest();
        });
    })

});


//
//var task = TaskFactory.create({
//    getProfile: function( id, done ){
//        task.parallel(
//            {
//                profile: function( next ){
//                    task.parallel({
//                        portraitProfile: function( next ){
//                            db.findOne({ _id: id }, next);
//                        },
//                        tgsProfile: function( next ) {
//                            db.findOne({ _id: id }, next);
//                        }
//                    }, next)();
//                },
//
//                trips: function( next ){
//                    db.find(null, next );
//                }
//            },
//            done);
//    }
//});
//
//
//task.getProfile = task.parallel({
//        getProfileThenBuildResponse: task.getPortaritProfileAndTrips.then( function( err, res, done ){
//            if(err)return done(err);
//            done(null, {
//                profile: {
//                    portrait: res.portraitProfile,
//                    tgs: res.tgsProfile
//                }
//            });
//        }),
//
//        getTripsByAccountId: function( done ){
//            db.find(null, done);
//        }
//    })
//    .then( function( err ,res, done ){
//        if(err)return done(err);
//        done(null, {
//            profile: res.getProfileThenBuildResponse,
//            trips: res.getTripsByAccountId
//        });
//    });
//
//task.getPortaritProfileAndTrips = task.parallel({
//    portraitProfile: function( next ){
//        db.findOne(null, next);
//    },
//    tgsProfile: function( next ) {
//        db.findOne(null, next);
//    }
//});
//
//task.getData();