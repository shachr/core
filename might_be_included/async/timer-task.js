/**
 * Created by shachar on 7/8/15.
 */
var util = require("util");

var TimeoutError = module.exports = function TimeoutError(){
    Error.call(this, "asynchronous operation timeout");
};

util.inherits(TimeoutError, Error);

exports = function( timeout, callback ){
    var run, timer;
    if(!timeout)throw new Error("required argument: timeout"); //todo: replace with assert

    run = function ( err, res ) {
        clearTimeout(timer);
        timer = null;
        if (err instanceof TimeoutError) {
            // note: incase its the timeout then res is the callback
            res.call(this, err);
        }
        else{
            callback.call(this, err, res);
        }
    };

    timer = setTimeout(run, timeout, new TimeoutError(), callback);
    return run;
};

