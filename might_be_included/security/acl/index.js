/**
 * Created by shachar on 12/16/14.
 */
var extend = require('node.extend');
module.exports = {

    roles: {
        "guest" : {
            id: "guest",
            aclId: ["guest"] // reference to acls[ id ]
        },
        "member" : {
            id: "member",
            aclId: ["guest", "member"] // reference to acls[ id ]
        }
    },


    acls: {
        'guest': {
            resources: {
                '/reviews/postReview': { perms: ['x'] }
            }
        },
        'member': {
            inherit: ["guest"],
            resources: {
                '/reviews/postReview/user': { perms: ['x'] },
                '/reviews/postReview/user/name': { perms: ['x'] },
                '/reviews/postReview/user/age': { perms: ['x'] }
            }
        }
    },



    isAllowed: function( roles, resourceId, perms ){
        if( !(roles instanceof Array ) )
            roles = [ roles ];

        if( !(perms instanceof Array ) )
            perms = [ perms ];

        var isAllowed = false;

        roles.forEach( function (roleId) {
            if(!roleId) return;
            var role = this.roles[ roleId ];
            var aclId = role.aclId[0];
            acl = this.acls[ aclId ];
            if(!acl) return;

            var aclsToMerge = [ true, acl ];
            if( acl.inherit && acl.inherit.length ){
                acl.inherit.forEach( function( aclId ){
                    var iAcl = this.acls[ aclId ];
                    if(!iAcl) return;
                    aclsToMerge.push( iAcl )
                }.bind( this ));

            }

            //clone & merge all acls.
            // todo: recursivly inherit!
            acl = extend.apply( extend, aclsToMerge );

            var resource = acl.resources[ resourceId ];
            if(!resource) return;

            if( !resource.perms) return;

            isAllowed = perms.length > 0;
            perms.forEach( function( perm){
                if( !~resource.perms.indexOf( perm )){
                    isAllowed = false;
                }
            });
        }.bind(this) );

        return isAllowed;
    }

};